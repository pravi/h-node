<?php

//created by Antonio Gallo (tonicucoz@yahoo.com)
// this script is in the Public Domain

$host		=	'';
$user		=	'';
$password	=	'';
$dbName		=	'';

class DB
{
	public static $instance;
}

DB::$instance = new mysqli($host,$user,$password,$dbName);

if (mysqli_connect_error())
{
	die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
}

?>