<?php

//created by Antonio Gallo (tonicucoz@yahoo.com)
// this script is in the Public Domain

function decode($url)
{
	$url = str_replace(' ','-',$url);
	$url = str_replace(',','',$url);
	$url = str_replace('\'','',$url);
	$url = str_replace('"','',$url);
	$url = str_replace('#','-',$url);
	$url = str_replace('[','',$url);
	$url = str_replace(']','',$url);
	$url = str_replace('(','',$url);
	$url = str_replace(')','',$url);
	$url = str_replace('/','-',$url);
	$url = str_replace('@','-at-',$url);
	$url = str_replace('?','-',$url);
	$url = str_replace('&','-',$url);
	return $url;
}

function decode_soft($url)
{
	$url = str_replace('\'','',$url);
	$url = str_replace('"','',$url);
	return $url;
}

function sanitizeDb($string) {

	$string = DB::$instance->real_escape_string($string);
	return $string;

}
