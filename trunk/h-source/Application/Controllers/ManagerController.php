<?php 

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

//this controller is used to carry out the actions of the superadmin
class ManagerController extends BaseController
{

	public function __construct($model, $controller, $queryString)
	{
   		parent::__construct($model, $controller, $queryString);
		
		$this->model('DistrosModel');
		$this->modelName = 'DistrosModel';
		
		$data['title'] = 'Manager - '.Website::$generalName;
		$this->append($data);
		
		$this->setArgKeys(array('page:forceInt'=>1));

		$this->s['registered']->check('manager');
	}
	
	public function distros($lang = 'en') { //view all the users

		$this->shift(1);
		
		$this->loadScaffold('main',array('popup'=>true,'popupType'=>'inclusive','recordPerPage'=>30, 'mainAction'=>'distros/'.$this->lang, "mainMenu" => "add"));

		$this->scaffold->loadMain('distros.full_name,distros.clean_name','distros.id_distro','moveup,movedown,ledit,del');
		$this->scaffold->setHead(gtext('TITLE').','.gtext('DISTRO-CODE'));

		$this->scaffold->mainMenu->links['add']['url'] = 'form/insert/0';
		$this->scaffold->mainMenu->links['add']['title'] = gtext('Insert a new distro');
		
		$this->scaffold->update('del');
		
		$this->scaffold->model->clear();

		$this->scaffold->model->orderBy("id_order");
			
		$this->scaffold->update('moveup,movedown');

		$data['scaffold'] = $this->scaffold->render();
// 		echo $this->scaffold->model->getQuery();
		
		$data['notice'] = $this->scaffold->model->notice;
		
		$this->append($data);
		$this->cleverLoad('distros');
		$this->right($this->lang);
	}

}