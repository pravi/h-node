<?php 

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class PrintersController extends GenericController
{

	public function __construct($model, $controller, $queryString)
	{

		parent::__construct($model, $controller, $queryString);
		
		//load the model
		$this->model('HardwareModel');
		$this->model('RevisionsModel');
		$this->model('PrintersModel');
		$this->model('TalkModel');
		
		$this->mod = $this->m['PrintersModel'];
		
		$this->m['HardwareModel']->id_user = $this->s['registered']->status['id_user'];
		$this->m['HardwareModel']->type = 'printer';

		//hardware conditions
		$this->m['HardwareModel']->strongConditions['update'] = array(
			"checkNotEmpty"	=>	"model|you have to fill the <i>model name</i> entry",
			"checkMatch|".Hardware::$regExpressions['model']	=>	"model|characters not allowed in the <i>model name</i> entry",
			"+checkMatch|".Hardware::$regExpressions['vendorid_productid']		=>	"pci_id|<i>VendorID:ProductID</i> has to have the following format: [a-zA-Z0-9]{4}(\:)[a-zA-Z0-9]{4}",
			"checkLength|190"	=>	"model",
			"+checkLength|299"	=>	"distribution",
			"+checkIsStrings|".Printer::compatibilityList() 	=> 	"compatibility",
			"++checkIsStrings|".Hardware::getCommYears()	=> 	"comm_year",
			"+++checkIsStrings|".Printer::$interface	=> 	"interface",
			"++++checkIsStrings|".Printer::$subtype	=> 	"subtype",
			"+++++checkIsStrings|".Printer::$trackSelect	=> 	"it_tracks_users",
		);

		$this->m['HardwareModel']->strongConditions['insert'] = $this->m['HardwareModel']->strongConditions['update'];

		$this->m['HardwareModel']->softConditions['update'] = array(
			"checkMatch|".Hardware::$regExpressions['kernel']	=>	"kernel|characters not allowed in the <i>kernel</i> entry",
			"checkLength|40000"	=>	"description",
			"+checkLength|49"	=>	"driver",
			"++checkLength|99"	=>	"kernel",
			"++checkMatch|".Hardware::$regExpressions['driver']	=>	"driver|only the following characters are allowed for the <i>driver</i> entry: a-z A-Z 0-9 - _ . + s / , : ; ( ) [ ]",
			"+++checkLength|1000"	=>	"other_names|the <i>other names</i> entry exceeds the value of 1000 characters",
		);
		
		$this->m['HardwareModel']->softConditions['insert'] = $this->m['HardwareModel']->softConditions['update'];
		
		$this->m['HardwareModel']->setFields('model,kernel,description,compatibility,distribution,comm_year,pci_id,driver,interface,subtype,other_names,it_tracks_users','sanitizeAll');
		
		$argKeys = array(
			'page:forceNat'					=>	1,
			'history_page:forceNat'			=>	1,
			'vendor:sanitizeString'			=>	'undef',
			'compatibility:sanitizeString'	=>	'undef',
			'comm_year:sanitizeString'		=>	'undef',
			'interface:sanitizeString'		=>	'undef',
			'sort-by:sanitizeString'		=>	'undef',
			'search_string:sanitizeString'	=>	'undef'
		);

		$this->setArgKeys($argKeys);
		
		$data['title'] = 'printers';

		$data['intefaceOptions'] = Printer::$interface;
		$data['worksOptions'] = Printer::$compatibility;
		$data['worksField'] = 'compatibility';

		$data['notFoundString'] = "No printers found";
		
		$data['subtypeHelpLabel'] = "laser, inkjet, ..";
		
		$this->append($data);
	}
	
	public function catalogue($lang = 'en')
	{		
		$this->shift(1);
		
		$whereArray = array(
			'type'			=>	$this->mod->type,
			'vendor'		=>	$this->viewArgs['vendor'],
			'compatibility'	=>	$this->viewArgs['compatibility'],
			'comm_year'		=>	$this->viewArgs['comm_year'],
			'interface'		=>	$this->viewArgs['interface'],
		);
		
		$this->mod->setWhereQueryClause($whereArray);
		
		parent::catalogue($lang);
	}

	public function view($lang = 'en', $id = 0, $name = null)
	{
		parent::view($lang, $id, $name);
	}

	public function history($lang = 'en', $id = 0)
	{
		parent::history($lang, $id);
	}

	public function revision($lang = 'en', $id_rev = 0)
	{
		parent::revision($lang, $id_rev);
	}

	public function insert($lang = 'en', $token = '')
	{
		parent::insert($lang, $token);
	}
	
	public function update($lang = 'en', $token = '')
	{
		parent::update($lang, $token);
	}

	public function differences($lang = 'en', $id_hard = 0, $id_rev = 0)
	{
		parent::differences($lang, $id_hard, $id_rev);
	}

	public function climb($lang = 'en', $id_rev = 0, $token = '')
	{
		parent::climb($lang, $id_rev, $token);
	}

	public function talk($lang = 'en', $id_hard = 0, $token = '')
	{
		parent::talk($lang, $id_hard, $token);
	}

}