<?php 

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class NotebooksController extends GenericController
{

	public function __construct($model, $controller, $queryString)
	{

		parent::__construct($model, $controller, $queryString);
		
		//load the model
		$this->model('HardwareModel');
		$this->model('RevisionsModel');
		$this->model('NotebooksModel');
		$this->model('TalkModel');
		
		$this->mod = $this->m['NotebooksModel'];
		
		$this->m['HardwareModel']->id_user = $this->s['registered']->status['id_user'];
		$this->m['HardwareModel']->type = 'notebook';

		//hardware conditions
		$this->m['HardwareModel']->strongConditions['update'] = array(
			"checkIsStrings|".Notebooks::vendorsList() 	=> 	"vendor",
			"checkNotEmpty"	=>	"model|you have to fill the <i>model name</i> entry",
			"checkMatch|".Hardware::$regExpressions['model']	=>	"model|characters not allowed in the <i>model name</i> entry",
			"+checkIsStrings|".Notebooks::compatibilityList() 	=> 	"compatibility",
			"checkLength|190"	=>						"model",
			"+checkLength|299"							=>	"distribution",
			"++checkIsStrings|".Hardware::getCommYears()	=> 	"comm_year",
			"+++checkIsStrings|".Notebooks::$subtypeSelect	=> 	"subtype",
			"++++checkIsStrings|".Notebooks::wifiList() 	=> 	"wifi_works",
			"+++++checkIsStrings|".Notebooks::videoList() 	=> 	"video_card_works",
			"++++++checkIsStrings|".Notebooks::biosList() 	=> 	"bios",
			"+++++++checkIsStrings|".Notebooks::webcamList() 	=> 	"webcam_works",
			"++++++++checkIsStrings|".Notebooks::architectureList() 	=> 	"architecture",
			"+++++++++checkIsStrings|".Notebooks::installableList() 	=> 	"can_free_systems_be_installed",
			"++++++++++checkIsStrings|".Notebooks::preventWifiList() 	=> 	"prevent_wifi",
		);

		$this->m['HardwareModel']->strongConditions['insert'] = $this->m['HardwareModel']->strongConditions['update'];

		$this->m['HardwareModel']->softConditions['update'] = array(
			"checkMatch|".Hardware::$regExpressions['kernel']	=>	"kernel|characters not allowed in the <i>kernel</i> entry",
			"checkLength|40000"							=>	"description",
			"++checkLength|99"							=>	"video_card_type,wifi_type",
			"+++checkLength|99"							=>	"kernel",
			"+checkMatch|/^[a-zA-Z0-9\-\_\.\+\s\/\,\:\;\(\)\[\]]+$/"	=>	"video_card_type|only the following characters are allowed for the <i>videocard</i> entry: a-z A-Z 0-9 - _ . + / , : ; ( ) [ ]",
			"++checkMatch|/^[a-zA-Z0-9\-\_\.\+\s\/\,\:\;\(\)\[\]]+$/"	=>	"wifi_type|only the following characters are allowed for the <i>wifi</i> entry: a-z A-Z 0-9 - _ . + / , : ; ( ) [ ]",
			"+++checkMatch|/^[a-zA-Z0-9\-\_\.\+\s\/\,\:\;\(\)\[\]]+$/"	=>	"webcam_type|only the following characters are allowed for the <i>webcam</i> entry: a-z A-Z 0-9 - _ . + / , : ; ( ) [ ]",
		);
		
		$this->m['HardwareModel']->softConditions['insert'] = $this->m['HardwareModel']->softConditions['update'];
		
		$this->m['HardwareModel']->setFields('vendor,model,compatibility,kernel,description,distribution,video_card_type,video_card_works,wifi_type,wifi_works,comm_year,subtype,bios,can_free_systems_be_installed,webcam_type,webcam_works,architecture,prevent_wifi','sanitizeAll');		
		
		$argKeys = array(
			'page:forceNat'					=>	1,
			'history_page:forceNat'			=>	1,
			'vendor:sanitizeString'			=>	'undef',
			'compatibility:sanitizeString'	=>	'undef',
			'comm_year:sanitizeString'		=>	'undef',
			'subtype:sanitizeString'		=>	'undef',
			'sort-by:sanitizeString'		=>	'undef',
			'bios:sanitizeString'			=>	'undef',
			'architecture:sanitizeString'	=>	'undef',
			'search_string:sanitizeString'	=>	'undef'
		);

		$this->setArgKeys($argKeys);

		$data['worksField'] = 'compatibility';
		
		$data['notFoundString'] = "No notebooks found";
		
		$data['title'] = 'Notebooks';
		$this->append($data);
	}
	
	public function catalogue($lang = 'en')
	{		
		$this->shift(1);
		
		$whereArray = array(
			'type'			=>	$this->mod->type,
			'vendor'		=>	$this->viewArgs['vendor'],
			'comm_year'		=>	$this->viewArgs['comm_year'],
			'subtype'		=>	$this->viewArgs['subtype'],
			'compatibility'	=>	$this->viewArgs['compatibility'],
			'bios'			=>	$this->viewArgs['bios'],
			'architecture'	=>	$this->viewArgs['architecture'],
		);
		
		$this->mod->setWhereQueryClause($whereArray);
		
		parent::catalogue($lang);
	}

	public function view($lang = 'en', $id = 0, $name = null)
	{
		parent::view($lang, $id, $name);
	}

	public function history($lang = 'en', $id = 0)
	{
		parent::history($lang, $id);
	}

	public function revision($lang = 'en', $id_rev = 0)
	{
		parent::revision($lang, $id_rev);
	}

	public function insert($lang = 'en', $token = '')
	{
		parent::insert($lang, $token);
	}
	
	public function update($lang = 'en', $token = '')
	{
		parent::update($lang, $token);
	}

	public function differences($lang = 'en', $id_hard = 0, $id_rev = 0)
	{
		parent::differences($lang, $id_hard, $id_rev);
	}

	public function climb($lang = 'en', $id_rev = 0, $token = '')
	{
		parent::climb($lang, $id_rev, $token);
	}

	public function talk($lang = 'en', $id_hard = 0, $token = '')
	{
		parent::talk($lang, $id_hard, $token);
	}

}