<?php 

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class MyController extends BaseController
{

	public function __construct($model, $controller, $queryString) {
		parent::__construct($model, $controller, $queryString);
		
		$this->model('UsersModel');
		$this->model('ProfileModel');
		
		$argKeys = array(
			'token:sanitizeAlphanum'	=>	'token'
		);

		$this->setArgKeys($argKeys);
		
		$data['title'] = 'my panel';
		$this->append($data);
	}
	
	public function home($lang = 'en')
	{
		$this->shift(1);
		
		$data['title'] = 'my panel - '.Website::$generalName;
		
		$this->s['registered']->check();
		
		$clean['id_user'] = (int)$this->s['registered']->status['id_user'];
		$data['username'] = $this->m['UsersModel']->getUser($clean['id_user']);
		
		$data['userBlocked'] = $this->m['UsersModel']->isBlocked($clean['id_user']);
		
		$this->append($data);
		$this->load('panel');
		$this->right($this->lang);
	}

	public function password($lang = 'en')
	{
		$this->shift(1);
		
		$data['title'] = 'password - '.Website::$generalName;
		
		$this->s['registered']->check();
		
		if (!$this->s['registered']->checkCsrf($this->viewArgs['token'])) $this->redirect($this->controller.'/home/'.$this->lang,2,'wrong token..');
		
		$this->m['UsersModel']->setFields('password:sha1','none');

		$this->m['UsersModel']->strongConditions['update'] = array('checkEqual'=>'password,confirmation');
		
		$data['notice'] = null;
		
		$clean['id_user'] = (int)$this->s['registered']->status['id_user'];
		
		if (isset($_POST['updateAction'])) {
			$pass = $this->s['registered']->getPassword();
			if (sha1($_POST['old']) === $pass)
			{
				$this->m['UsersModel']->updateTable('update',$clean['id_user']);
				$data['notice'] = $this->m['UsersModel']->notice;
				if ($this->m['UsersModel']->queryResult)
				{
					$this->s['registered']->logout();
					$this->redirect('home/index/'.$this->lang,2,'logout');
				}
			}
			else
			{
				$data['notice'] = "<div class='alert'>The old password is wrong</div>\n";
			}
		}

		$values = $this->m['UsersModel']->selectId($clean['id_user']);
		$values['old'] = '';
		$values['confirmation'] = '';
		
		$action = array('updateAction'=>'save');
		$form = new Form_Form('my/password/'.$this->lang.$this->viewStatus,$action);
		$form->setEntry('old','Password');
		$form->entry['old']->labelString = 'old password:';
		$form->setEntry('password','Password');
		$form->setEntry('confirmation','Password');
		$data['form'] = $form->render($values,'old,password,confirmation');

		$this->append($data);
		
		$this->load('password');
		$this->right();
	}
	
	public function email($lang = 'en')
	{
		$this->shift(1);
		
		$data['title'] = 'email - '.Website::$generalName;
		
		$this->s['registered']->check();
		
		if (!$this->s['registered']->checkCsrf($this->viewArgs['token'])) $this->redirect($this->controller.'/home/'.$this->lang,2,'wrong token..');
		
		$this->m['UsersModel']->setFields('e_mail','sanitizeAll');

		$this->m['UsersModel']->strongConditions['update'] = array('checkMail'=>'e_mail');
		
		$this->m['UsersModel']->databaseConditions['update'] = array('checkUniqueCompl'=>'e_mail');
		
		$data['notice'] = null;
		
		$clean['id_user'] = (int)$this->s['registered']->status['id_user'];

		$this->m['UsersModel']->updateTable('update',$clean['id_user']);
		$data['notice'] = $this->m['UsersModel']->notice;
		
		$values = $this->m['UsersModel']->selectId($clean['id_user']);
		
		$action = array('updateAction'=>'save');
		$form = new Form_Form('my/email/'.$this->lang.$this->viewStatus,$action);
		$form->setEntry('e_mail','InputText');
		$form->entry['e_mail']->labelString = 'your e-mail address:';
		$data['form'] = $form->render($values,'e_mail');
		
		$this->append($data);
		
		$this->load('email');
		$this->right();
	}

	public function profile($lang = 'en')
	{
		$this->shift(1);
		
		$data['title'] = 'profile - '.Website::$generalName;
		
		$this->s['registered']->check();
		
		if (!$this->s['registered']->checkCsrf($this->viewArgs['token'])) $this->redirect($this->controller.'/home/'.$this->lang,2,'wrong token..');
		
		$this->m['ProfileModel']->setFields('real_name,website,where_you_are,birth_date,fav_distro,projects,publish_mail,send_notification,description','sanitizeAll');
		
		$clean['id_user'] = (int)$this->s['registered']->status['id_user'];
		
		$res = $this->m['ProfileModel']->db->select('profile','id_prof','created_by='.$clean['id_user']);
		$clean['id_prof'] = (int)$res[0]['profile']['id_prof'];
		
		$this->m['ProfileModel']->values['update_date'] = date('Y-m-d H:i:s');
		$this->m['ProfileModel']->updateTable('update',$clean['id_prof']);
		$data['notice'] = $this->m['ProfileModel']->notice;
		
		$values = $this->m['ProfileModel']->getFormValues('update','sanitizeHtml',$clean['id_prof']);
		
		$this->m['ProfileModel']->setForm('my/profile/'.$this->lang.$this->viewStatus,array('updateAction'=>'save'),'POST');
		$data['form'] = $this->m['ProfileModel']->form->render($values);
		
		$this->append($data);
		
		$this->load('profile');
		$this->right();
	}
	
	public function goodbye($lang = 'en')
	{
		$data['title'] = 'delete - '.Website::$generalName;
		
		session_start();
		
		$this->shift(1);
		
		$this->s['registered']->check();
		
		if (!$this->s['registered']->checkCsrf($this->viewArgs['token'])) $this->redirect($this->controller.'/home/'.$this->lang,2,'wrong token..');
		
		$clean['id_user'] = (int)$this->s['registered']->status['id_user'];
		
		if (isset($_POST['closeAction']))
		{
			$this->s['registered']->logout();
			$this->m['UsersModel']->close($clean['id_user']);
			
			if ($this->m['UsersModel']->queryResult)
			{
				$this->redirect('users/notice/'.$this->lang);
			}

		}
		
		$this->append($data);
		$this->load('goodbye');
		$this->right();
	}
	
}