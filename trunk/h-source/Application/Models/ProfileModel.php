<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class ProfileModel extends Model_Tree {

	public function __construct() {
		$this->_tables = 'profile';
		$this->_idFields = 'id_prof';
		
		$this->_where=array(
			'username'		=>	'regusers',
			'has_confirmed'	=>	'regusers',
			'deleted'		=>	'regusers'
		);
		
		$this->softConditions['update'] = array(
			"checkLength|90"	=>	"real_name,where_you_are,fav_distro,birth_date,website|the fields 'real name', 'where_you_are', 'favorite distro', 'website' and 'birthdate' don't have to have more than 90 characters",
			"checkLength|1000"	=>	"projects,description|the fields 'projects' and 'description' don't have to have more than 1000 characters",
			"checkIsStrings|no,yes"	=>	"publish_mail",
			"+checkIsStrings|no,yes"	=>	"send_notification"
		);
		
		parent::__construct();
	}

	public $formStruct = array(
	
		'entries' 	=> 	array(
			'real_name'	=> 	array('labelString'=>'Your real name'),
			'website'	=>	array('labelString'=>'Your website address (add http://)'),
			'where_you_are'	=> 	array('labelString'=>'I\'m from...'),
			'birth_date'	=> 	array('labelString'=>'My birthdate'),
			'fav_distro'	=> 	array('labelString'=>'My favourite distribution'),
			'projects'	=>	array(
				'type'		=>	'Textarea',
				'labelString'=>'Free software projects I\'m working on'
			),
			'publish_mail'		=>	array(
				'type'		=>	'Select',
				'options'	=>	'no,yes',
				'labelString'=>	'Would you like to publish your e-mail address?'
			),
			'send_notification'		=>	array(
				'type'		=>	'Select',
				'options'	=>	'yes,no',
				'labelString'=>	'Would you like to receive mail notifications?'
			),
			'description'	=>	array(
				'type'		=>	'Textarea',
				'labelString'=>	'Your description'
			),
			'id_prof'		=>	array(
				'type'		=>	'Hidden'	
			)
		),
	
	);
	
}