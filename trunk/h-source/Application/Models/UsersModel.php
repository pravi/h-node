<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class UsersModel extends Model_Map
{

	public static $usersList = array();
	
	public function __construct()
	{
		$this->_tables='regusers,reggroups,regusers_groups';
		$this->_idFields='id_user,id_group';
		
		$this->_where=array(
			'id_group'			=>	'reggroups',
			'id_user'			=>	'regusers',
			'name'				=>	'reggroups',
			'confirmation_token'=>	'regusers',
			'has_confirmed'		=>	'regusers',
			'deleted'			=>	'regusers',
			'forgot_token'		=>	'regusers'
		);
		
		$this->_popupItemNames = array(
			'has_confirmed'=>'has_confirmed',
			'deleted'=>'deleted',
			'id_group'=>'name',
		);

		$this->_popupLabels = array(
			'has_confirmed'=>'HAS CONFIRMED?',
			'deleted'=>'DELETED?',
			'id_group'=>'GROUP'
		);
		
		$this->orderBy = 'regusers.id_user desc';
		
		parent::__construct();
		
		$this->deleteNotRegistered();
	}

	public function pUpdate($id)
	{
		return parent::update($id);
	}
	
	public function deleteNotRegistered()
	{
		$limit = time() - Account::$confirmTime; 
		$this->db->del('regusers','has_confirmed = 1 and deleted = "no" and creation_time < '.$limit);
	}

	public function getUser($id_user = 0)
	{
		$clean['id_user'] = (int)$id_user;
		if (array_key_exists($clean['id_user'],self::$usersList))
		{
			return self::$usersList[$clean['id_user']];
		}
		else
		{
			$user = $this->db->select('regusers','username,has_confirmed','id_user='.$clean['id_user']);
			if (count($user) > 0)
			{
				$fuser = (strcmp($user[0]['regusers']['has_confirmed'],0) === 0) ? $user[0]['regusers']['username'] : "__".$user[0]['regusers']['username'];
				self::$usersList[$clean['id_user']] = $fuser;
				return $fuser;
			}
			else
			{
				return "<i>__anonymous__</i>";
			}
		}
	}

	public function getLinkToUser($user)
	{
		if (strstr($user,'__'))
		{
			return $user;
// 			return str_replace('__',null,$user);
		}
		else
		{
			return "<a href='".Url::getRoot()."meet/user/".Lang::$current."/$user'>$user</a>";
		}
	}

	public function getLinkToUserFromId($id_user = 0)
	{
		$clean['id_user'] = (int)$id_user;
		return $this->getLinkToUser($this->getUser($clean['id_user']));
	}

	//check if the user exists
	public function userExists($user)
	{
		$clean['user'] = ctype_alnum($user) ? sanitizeAll($user) : '';

		if (strcmp($clean['user'],'') !== 0)
		{
			$res = $this->where(array("username"=>$clean['user'],"has_confirmed"=>"0","deleted"=>"no"))->send();
// 			$res = $this->db->select('regusers','has_confirmed','username="'.$clean['user'].'" and has_confirmed=0 and deleted="no"');

			if (count($res) > 0)
			{
				return true;
			}
		}
		return false;
	}

	//get the user id from the username
	public function getUserId($username = '')
	{
		$clean['username'] = ctype_alnum($username) ? sanitizeAll($username) : '';
		
		$users = $this->select('id_user')->where(array('username'=>$clean['username'],'has_confirmed'=>0,'deleted'=>'no'))->send();
		if (count($users) > 0)
		{
			return $users[0]['regusers']['id_user'];
		}
		else
		{
			return 0;
		}
	}

	public function isBlocked($idUser)
	{
		$clean['id_user'] = (int)$idUser;
		
		$res = $this->select('blocked')->where(array('id_user'=>$clean['id_user'],'has_confirmed'=>0,'deleted'=>'no'))->toList('blocked')->send();
		
		if (count($res) > 0)
		{
			return strcmp($res[0],'yes') === 0 ? true : false;
		}
		
		return true;
	}
	
	public function insert()
	{
		//create the token
		$confirmation_token = md5(randString(20));
		$this->values['confirmation_token'] = $confirmation_token;
		//has_confirmed flag
		$this->values['has_confirmed'] = 1;
		$this->values['creation_time'] = time();
		
		//random ID
		$randomId = md5(randString(5).uniqid(mt_rand(),true));
		$this->values["temp_field"] = $randomId;
		
		if (isset($_POST['captcha']))
		{
			if ( strcmp($_SESSION['captchaString'],$_POST['captcha']) === 0 )
			{
				
				parent::insert();
				
				if ($this->queryResult)
				{
					$resId = $this->db->select("regusers","id_user","temp_field='$randomId'");
					$clean['id_user'] = $resId[0]['regusers']['id_user'];
					$this->db->update("regusers",'temp_field',array(''),'id_user='.$clean['id_user']);
					
					$result = Account::confirm($this->values['username'],$this->values['e_mail'],$clean['id_user'],$confirmation_token);
					
					if ($result)
					{
						$_SESSION['status'] = 'sent';
					}
					else
					{
						$_SESSION['status'] = 'regerror';
					}
					
					$hed = new HeaderObj(DOMAIN_NAME);
					$hed->redirect('users/notice/'.Lang::$current);
				}
			
			}
			else
			{
				$this->result = false;
				$this->queryResult = false;
				$this->notice = "<div class='alert'>Wrong captcha code...</div>\n";
			}
		}
	}

	public function close($id_user)
	{
		$clean['id_user'] = (int)$id_user;
		
		$this->values = array(
			'has_confirmed'	=>	1,
			'deleted'		=>	'yes',
			'e_mail'		=>	''
		);
		
		if ($this->update($clean['id_user']))
		{
			$_SESSION['status'] = 'deleted';
			
			$profile = new ProfileModel();
			$res = $profile->db->select('profile','id_prof','created_by='.$clean['id_user']);
			
			if (count($res) > 0)
			{
				$clean['id_prof'] = (int)$res[0]['profile']['id_prof'];
				$profile->values = array(
					'real_name'		=>	'',
					'where_you_are'	=>	'',
					'birth_date'	=>	'',
					'fav_distro'	=>	'',
					'projects'		=>	'',
					'description'	=>	''
				);
				$profile->update($clean['id_prof']);
			}
		}
		
	}

	public function forgot($username)
	{
		$clean['username'] = ctype_alnum($username) ? sanitizeAll($username) : '';
		
		if (isset($_POST['captcha']))
		{
			if ( strcmp($_SESSION['captchaString'],$_POST['captcha']) === 0 )
			{
				$res = $this->db->select('regusers','e_mail,id_user','username="'.$clean['username'].'" and has_confirmed = 0 and deleted = "no"');
				if (count($res) > 0)
				{
					$e_mail = $res[0]['regusers']['e_mail'];
					$id_user = (int)$res[0]['regusers']['id_user'];
					$forgot_token = md5(randString(20));
					$forgot_time = time();
					$updateArray = array($forgot_token, $forgot_time);
					$this->db->update('regusers','forgot_token,forgot_time',$updateArray,'username="'.$clean['username'].'"');
					
					$result = Account::sendnew($clean['username'],$e_mail,$id_user,$forgot_token);
					
					if ($result)
					{
						$_SESSION['status'] = 'sent_new';
					}
					else
					{
						$_SESSION['status'] = 'sent_new_error';
					}
					
					$hed = new HeaderObj(DOMAIN_NAME);
					$hed->redirect('users/notice/'.Lang::$current,1);
					
				}
				else
				{
					$this->notice = "<div class='alert'>the user does not exist</div>\n";
				}
			}
			else
			{
				$this->result = false;
				$this->queryResult = false;
				$this->notice = "<div class='alert'>Wrong captcha code...</div>\n";
			}
		}
	}

}