<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class MessagesModel extends BaseModel {

	public function __construct() {
		$this->_tables = 'messages';
		$this->_idFields = 'id_mes';
		
		$this->orderBy = 'messages.id_mes';
		
		$this->_popupItemNames = array(
			'deleted'	=>	'deleted',
			'has_read'	=>	'has_read',
		);
		
		$this->_popupLabels = array(
			'deleted'	=>	'DELETED?',
			'has_read'	=>	'ALREADY READ?',
		);
		
		$this->strongConditions['insert'] = array(
			"checkLength|5000"	=>	'message',
		);
		
		parent::__construct();
	}

	public function pUpdate($id)
	{
		return parent::update($id);
	}

	public function insert()
	{
		if (parent::insert())
		{
			//update the history
			$this->updateHistory('message_ins');
		}
	}
	
	public $formStruct = array(
		'entries' 	=> 	array(
			'deleted'	=> 	array('type'=>'Select','options'=>'no,yes'),
			'has_read'	=> 	array('type'=>'Select','options'=>'no,yes'),
			'message'	=> 	array('type'=>'Textarea','idName'=>'bb_code'),
			'id_mes'		=>	array(
				'type'		=>	'Hidden'	
			)
		),
	);

}