<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class TalkModel extends BaseModel {

	public function __construct() {
		$this->_tables = 'talk';
		$this->_idFields = 'id_talk';
		
		$this->_where=array(
			'id_hard'			=>	'talk',
			'has_confirmed'		=>	'regusers',
			'-deleted'			=>	'regusers',
		);
		
		$this->orderBy = 'talk.id_talk desc';
		
		$this->strongConditions['insert'] = array(
			"checkLength|99"	=>	'title',
			"+checkNotEmpty"	=>	'message',
			"++checkLength|5000"	=>	'message',
		);
		
		parent::__construct();
	}

	public function pUpdate($id)
	{
		return parent::update($id);
	}
	
	public function insert()
	{
		if (parent::insert())
		{
			//update the history
			$this->updateHistory('talk_ins');
				
			if (strcmp($this->values['id_hard'],0) !== 0)
			{
				$clean['id_hard'] = (int)$this->values['id_hard'];
				$clean['created_by'] = (int)$this->values['created_by'];

				$hard = new HardwareModel();
				$users = $hard->select('username,e_mail')
					->where(array('id_hard'=>$clean['id_hard'],'has_confirmed'=>0,'deleted'=>'no'))
					->toList('regusers.username','regusers.e_mail')
					->send('Boxes');

				$talkUsers = $this->select('username,e_mail')
					->from('talk inner join regusers')
					->on('talk.created_by = regusers.id_user')
					->where(array('id_hard'=>$clean['id_hard'],'has_confirmed'=>0,'-deleted'=>'no'))
					->toList('regusers.username','regusers.e_mail')
					->send();

				$allUsers = array_merge($users,$talkUsers);
				
				$userObj = new UsersModel();
				$myName = $userObj->getUser($clean['created_by']);

				$arrayE = new ArrayExt();
				$fusers = $arrayE->subsetComplementary($allUsers,$myName);

				//remove users that do not want the email notification
				$inString = "'".implode("','",array_keys($fusers))."'";
				$profile = new ProfileModel();
				$ffusers = $profile->select('username,e_mail')
					->from('regusers inner join profile')
					->on('regusers.id_user = profile.created_by')
					->where(array('send_notification'=>'yes','username'=>"in($inString)"))
					->toList('regusers.username','regusers.e_mail')
					->send();

				$ffusers = array_flip($ffusers);
				
				//send the notice
				Account::sendTalkNotice($myName,$ffusers,$clean['id_hard']);

				$domainName = rtrim(Url::getRoot(),"/");
				header('Refresh: 0;url='.$domainName.$_SERVER['REQUEST_URI']);
				exit;
			}
		}
	}

	public $formStruct = array(
		'entries' 	=> 	array(
			'title'	=> 	array(),
			'message'	=> 	array('type'=>'Textarea'),
			'id_talk'		=>	array(
				'type'		=>	'Hidden'	
			)
		),
	);

}