<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class RevisionsModel extends Model_Tree {

	public $id_user = 0;
	public $type = ''; //device type
	
	public function __construct() {
		$this->_tables='revisions';
		$this->_idFields='id_rev';
		
		$this->_where=array(
			'id_hard'=>'revisions'
		);
		
		$this->orderBy = 'id_rev desc';

		parent::__construct();
	}

	public function getIdHard($id_rev = 0)
	{
		$clean['id_rev'] = (int)$id_rev;
		
		$res = $this->db->select('revisions','id_hard','id_rev='.$clean['id_rev']);
		
		return (count($res) > 0) ? $res[0]['revisions']['id_hard'] : 0;
	}

}