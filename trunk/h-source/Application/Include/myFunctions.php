<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

function encodeUrl($url)
{
	$url = str_replace(' ','-',$url);
	$url = str_replace('.','-',$url);
	$url = str_replace(',','-',$url);
	$url = str_replace('[','-',$url);
	$url = str_replace(']','-',$url);
	$url = str_replace('(','-',$url);
	$url = str_replace(')','-',$url);
	$url = str_replace('/','-',$url);
	$url = str_replace('&#039;','-',$url);
	$url = str_replace('@','-at-',$url);
	$url = str_replace('?','-',$url);
	
// 	$temp = null;
// 	for ($i=0;$i<strlen($url); $i++)
// 	{
// 		if (strcmp($url[$i],' ') === 0)
// 		{
// 			$temp .= '-';
// 		}
// 		else
// 		{
// 			if (preg_match('/^[a-zA-Z0-9\-]$/',$url[$i])) $temp .= $url[$i];
// 		}
// 	}

// 	$url = urlencode($url);
// 	$url = urlencode($url);
	return $url;
}

function titleForRedirect($title)
{
	return html_entity_decode(encodeUrl($title),ENT_QUOTES,DEFAULT_CHARSET);
}

// function encodeUrl($url)
// {
// 	$url = str_replace(' ','-',$url);
// 	$url = str_replace('[','-',$url);
// 	$url = str_replace(']','-',$url);
// 	$url = str_replace('(','-',$url);
// 	$url = str_replace(')','-',$url);
// 	$url = str_replace('/','-',$url);
// 	$url = str_replace('@','-at-',$url);
// 	$url = urlencode($url);
// // 	$url = html_entity_decode($url, ENT_QUOTES);
// // 	$url = xml_encode($url);
// 	return $url;
// }


function smartDate($uglyDate = null, $lang = 'en')
{
	if (strcmp($lang,'en'))
	{
		return date('H:i, j F Y',strtotime($uglyDate));
	}
	else
	{
		return date('H:i, j ',strtotime($uglyDate)).gtext(date('F',strtotime($uglyDate))).date(' Y',strtotime($uglyDate));
	}
}

function pubDateFormat($uglyDate = null)
{
	return date('r',strtotime($uglyDate));
}

function sanitizeString($string)
{
	$string = preg_match('/^[a-zA-Z0-9\-\_\.\+\s]+$/',$string) ? sanitizeAll($string) : 'undef';
	return $string;
}

function sanitizeAlphanum($string)
{
	$string = ctype_alnum($string) ? sanitizeAll($string) : 'undef';
	return $string;
}

function sanitizePciid($string)
{
	$string = preg_match('/^[a-zA-Z0-9]{4}(\:)[a-zA-Z0-9]{4}$/',$string) ? sanitizeAll($string) : '0';
	return $string;
}

function getOrderByClause($string)
{
	switch ($string)
	{
		case 'last-inserted':
			$orderBy = 'hardware.id_hard desc';
			break;
		case 'alphabetically':
			$orderBy = 'model';
			break;
		case 'alphabetically-desc':
			$orderBy = 'model desc';
			break;
		case 'compatibility':
			$orderBy = 'compatibility';
			break;
		case 'undef':
			$orderBy = 'hardware.id_hard desc';
			break;
		default:
			$orderBy = 'hardware.id_hard desc';
	}
	
	return $orderBy;
}

//remove empty elements
function removeEmptyStrings($oArray)
{
	$nArray = array();
	foreach ($oArray as $oValue)
	{
		if (strcmp($oValue,'') !== 0)
		{
			$nArray[] = $oValue;
		}
	}
	return $nArray;
}

function diff($old, $new)
{
	$maxlen = 0;
	foreach($old as $oindex => $ovalue)
	{
			$nkeys = array_keys($new, $ovalue);
// 			echo memory_get_peak_usage(true)."<br />";
			foreach($nkeys as $nindex)
			{
					$matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ?
							$matrix[$oindex - 1][$nindex - 1] + 1 : 1;
					if($matrix[$oindex][$nindex] > $maxlen)
					{
							$maxlen = $matrix[$oindex][$nindex];
							$omax = $oindex + 1 - $maxlen;
							$nmax = $nindex + 1 - $maxlen;
					}
			}
	}
	if($maxlen == 0) return array(array('d'=>$old, 'i'=>$new));
	return array_merge(
			diff(array_slice($old, 0, $omax), array_slice($new, 0, $nmax)),
			array_slice($new, $nmax, $maxlen),
			diff(array_slice($old, $omax + $maxlen), array_slice($new, $nmax + $maxlen)));
}

function htmlDiff($old, $new)
{
	$old = str_replace("\r\n"," \r\n ",$old);
	$new = str_replace("\r\n"," \r\n ",$new);

	$ret = null;
	$diff = diff(removeEmptyStrings(explode(' ', $old)),removeEmptyStrings(explode(' ', $new)));
	foreach($diff as $k)
	{
			if(is_array($k))
					$ret .= (!empty($k['d'])?"<del>".implode(' ',$k['d'])."</del> ":'').
							(!empty($k['i'])?"<ins>".implode(' ',$k['i'])."</ins> ":'');
			else $ret .= $k . ' ';
	}
	return $ret;
}


function applyBreaks($values,$fields)
{
	$fieldsArray = explode(',',$fields);
	
	foreach ($fieldsArray as $field)
	{
		if (array_key_exists($field,$values))
		{
			$values[$field] = nl2br($values[$field]);
		}
	}
	return $values;
}


function getLinkToUser($user)
{
	if (strstr($user,'__'))
	{
		return str_replace('__',null,$user);
	}
	else
	{
		return "<a href='".Url::getRoot()."meet/user/".Lang::$current."/$user'>$user</a>";
	}
}

function getUserName($id_user = 0)
{
	$clean['id_user'] = (int)$id_user;
	$u = new UsersModel();
	return $u->getUser($clean['id_user']);
}

function getMotivation($row,$controller)
{
	if (strcmp($row['deletion']['object'],'duplication') === 0)
	{
		$clean['id_hard'] = (int)$row['deletion']['id_duplicate'];
		$hardware = new HardwareModel();
		$name = encodeUrl($hardware->getTheModelName($clean['id_hard']));
		return "<b>duplication</b> of the model having id <b><a href='".Url::getRoot().$controller."/view/".Lang::$current."/".$clean['id_hard']."/".$name."'>".$clean['id_hard']."</a></b>";
	}
	else
	{
		return "<b>".$row['deletion']['object']."</b>";
	}
}

//get the text in the right language
function gtext($string)
{
	if (isset(Lang::$i18n[Lang::$current][$string]))
	{
		return Lang::$i18n[Lang::$current][$string];
	}
	return $string;
}

function singular($string)
{
	if (isset(Lang::$singular[Lang::$current][$string]))
	{
		return Lang::$singular[Lang::$current][$string];
	}
	return $string;
}

function plural($string)
{
	if (isset(Lang::$plural[Lang::$current][$string]))
	{
		return Lang::$plural[Lang::$current][$string];
	}
	return $string;
}

//get the hardware info from the talk id
function getHardwareInfoFromTalkId($id = 0)
{
	$clean['id'] = (int)$id;
	$talk = new TalkModel();
	$res = $talk->select('hardware.type,hardware.id_hard')->from('hardware inner join talk')->using('id_hard')->where(array('id_talk'=>$clean['id']))->send();
	return count($res) > 0 ? $res[0]['hardware'] : null;
}

//get hardware info from id
function getHardwareInfoFromId($id = 0)
{
	$clean['id'] = (int)$id;
	$hw = new HardwareModel();
	$res = $hw->select()->where(array('id_hard'=>$clean['id']))->send();
	$controller = 'home/index/en';
	$model = '';
	if (count($res) > 0)
	{
		$controller = Hardware::getControllerFromType($res[0]['hardware']['type']);
		$controller = strcmp($controller,'') !== 0 ? $controller : 'home/index/en';
		$model = $res[0]['hardware']['model'];
	}
	
	return array('controller'=>$controller,'model'=>$model);
}

//get the wiki page info from the talk id
function getWikiPageInfoFromTalkId($id = 0)
{
	$clean['id'] = (int)$id;
	$talk = new WikitalkModel();
	$res = $talk->select('wiki.id_wiki')->from('wiki inner join wiki_talk')->using('id_wiki')->where(array('id_talk'=>$clean['id']))->send();
	return count($res) > 0 ? $res[0]['wiki']['id_wiki'] : '';
}

//get the issue info from the message id
function getIssueNumberFromMessageId($id = 0)
{
	$clean['id'] = (int)$id;
	$mess = new MessagesModel();
	$res = $mess->select('issues.id_issue')->from('issues inner join messages')->using('id_issue')->where(array('id_mes'=>$clean['id']))->toList('issues.id_issue')->send();
	return count($res) > 0 ? $res[0] : '';
}

//get thw wiki name from the id
function getWikiNameFromId($id = 0)
{
	$clean['id'] = (int)$id;
	$wiki = new WikiModel();
	$name = $wiki->getTheModelName($clean['id']);
	return $name;
}

//return the URL to the moderated object page
function goToModeratedItem( $row = array() )
{
	$url = null;
	
	switch ($row['type'])
	{
		case 'message':
			$url = 'issues/view/'.Lang::$current.'/'.getIssueNumberFromMessageId($row['id']).'#message-'.$row['id'];
			break;
		case 'talk':
			$hardInfo = getHardwareInfoFromTalkId($row['id']);
			if (isset($hardInfo))
			{
				$controller = Hardware::$typeToController[$hardInfo['type']];
				$url = $controller.'/talk/'.Lang::$current.'/'.$hardInfo['id_hard'].'#talk-'.$row['id'];
			}
			else
			{
				$url = 'last/modactions/'.Lang::$current;
			}
			break;
		case 'user':
			$url = 'meet/user/'.Lang::$current.'/'.getUserName($row['id']);
			break;
		case 'issue':
			$url = 'issues/view/'.Lang::$current.'/'.$row['id'];
			break;
		case 'issue_del':
			$url = 'issues/view/'.Lang::$current.'/'.$row['id'];
			break;
		case 'wiki_talk':
			$url = 'wiki/talk/'.Lang::$current.'/'.getWikiPageInfoFromTalkId($row['id']).'#wiki-talk-'.$row['id'];
			break;
		case 'page':
			$url = 'wiki/page/'.Lang::$current.'/'.encodeUrl(getWikiNameFromId($row['id']));
			break;
		case 'page_del':
			$url = 'wiki/page/'.Lang::$current.'/'.encodeUrl(getWikiNameFromId($row['id']));
			break;
		case 'device':
			$hardInfo = getHardwareInfoFromId($row['id']);
			$url = $hardInfo['controller'].'/view/'.Lang::$current.'/'.$row['id'].'/'.encodeUrl($hardInfo['model']);
			break;
		case 'device_app':
			$hardInfo = getHardwareInfoFromId($row['id']);
			$url = $hardInfo['controller'].'/view/'.Lang::$current.'/'.$row['id'].'/'.encodeUrl($hardInfo['model']);
			break;
		case 'device_cl':
			$hardInfo = getHardwareInfoFromId($row['id']);
			$url = $hardInfo['controller'].'/view/'.Lang::$current.'/'.$row['id'].'/'.encodeUrl($hardInfo['model']);
			break;
		case 'message_ins':
			$url = 'issues/view/'.Lang::$current.'/'.getIssueNumberFromMessageId($row['id']).'#message-'.$row['id'];
			break;
		case 'wiki_talk_ins':
			$url = 'wiki/talk/'.Lang::$current.'/'.getWikiPageInfoFromTalkId($row['id']).'#wiki-talk-'.$row['id'];
			break;
		case 'issue_ins':
			$url = 'issues/view/'.Lang::$current.'/'.$row['id'];
			break;
		case 'talk_ins':
			$hardInfo = getHardwareInfoFromTalkId($row['id']);
			if (isset($hardInfo))
			{
				$controller = Hardware::$typeToController[$hardInfo['type']];
				$url = $controller.'/talk/'.Lang::$current.'/'.$hardInfo['id_hard'].'#talk-'.$row['id'];
			}
			else
			{
				$url = 'last/modactions/'.Lang::$current;
			}
			break;
		case 'hardware':
			$hardInfo = getHardwareInfoFromId($row['id']);
			$url = $hardInfo['controller'].'/view/'.Lang::$current.'/'.$row['id'].'/'.encodeUrl($hardInfo['model']);
			break;
		case 'wiki':
			$url = 'wiki/page/'.Lang::$current.'/'.encodeUrl(getWikiNameFromId($row['id']));
			break;
	}
	return Url::getRoot($url);
}

function getUrlsFromIdHard($id_hard)
{
	$clean['id_hard'] = (int)$id_hard;
	
	$urlView = $urlTalk = Url::getRoot();

	$deviceName = null;
	
	$hard = new HardwareModel();
	$res = $hard->select('type,model')->where(array('id_hard'=>$clean['id_hard']))->send();
	if (count($res) > 0)
	{
		$urlView = Url::getRoot().Hardware::$typeToController[$res[0]['hardware']['type']]."/view/".Lang::$current."/".$clean['id_hard']."/".encodeUrl($res[0]['hardware']['model']);
		
		$urlTalk = Url::getRoot().Hardware::$typeToController[$res[0]['hardware']['type']]."/talk/".Lang::$current."/".$clean['id_hard'];

		$deviceName = $res[0]['hardware']['model'];
	}
	
	return array('urlView'=>$urlView,'urlTalk'=>$urlTalk,'modelName'=>$deviceName);
	
}

function getDiffArray($associativeArray, $oldArray, $newArray)
{
	$diffArray = array();
	foreach ($associativeArray as $field => $label)
	{
		if (array_key_exists($field,$oldArray) and array_key_exists($field,$newArray))
		{
			$diffArray[$label] = htmlDiff($oldArray[$field], $newArray[$field]);
		}
	}
	return $diffArray;
}

//get the translation function
function getTranslationFunction($fieldName)
{
	$fieldsFunc = array(
		"vendor"	=>	"betterVendor",
		"interface"	=>	"translate_and_gtext",
		"subtype"	=>	"translate_and_gtext",
	);

	if (array_key_exists($fieldName,$fieldsFunc))
	{
		return $fieldsFunc[$fieldName];
	}

	return "gtext";
}