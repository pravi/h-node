<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div data-role="content">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; credits
		</div>
		
		<div class="credits_external_box">
			
			<div class="credits_item_title">
				Icons:
			</div>
			
			<div class="credits_item_description">
				Les icones utilisées sur <?php echo Website::$generalName;?> sont tirées du thème <a href="http://kde-look.org/content/show.php/ACUN+Simgeleri?content=83018">ACUN Simgeleri 0.7</a> et du thème <a href="http://kde-look.org/content/show.php/H2O+Icon+Theme?content=127149">H2O Icon Theme 0.0.5</a>, les deux sont licensée sous license GPL, d’autres sont aussi tirée des thèmes <a href="http://www.everaldo.com/crystal/?action=downloads">Crystal Projects</a> (LGPL), <a href="http://www.notmart.org/index.php/Graphics">glaze icons set</a> (LGPL), <a href="http://kde-look.org/content/show.php/Dark-Glass+reviewed?content=67902">DarkGlass_Reworked icons theme</a> (GPL). Les icones de drapeau sont tirés du thème <a href="http://www.famfamfam.com/lab/icons/flags/">FAMFAMFAM flag icons set</a> distribué dans le domaine publique.
			</div>
			
			<div class="credits_item_title">
				jQuery:
			</div>
			
			<div class="credits_item_description">
				<a href="http://jquery.com/">jQuery</a> , <a href="http://jqueryui.com/home">jQuery UI</a> et la librairie javascript <a href="http://jquerymobile.com/">jQuery Mobile</a> (licensé sous MIT/GPL) sont utilisés sur le site.
			</div>

			<div class="credits_item_title">
				markitup:
			</div>
			
			<div class="credits_item_description">
				Le plugin jQuery <a href="http://markitup.jaysalvat.com/home/">markitup</a> (licensé sous MIT/GPL) a été utilisé afin d’aider les utilisateurs à insérer des tags wiki.
			</div>
			
			<div class="credits_item_title">
				Algorithme diff php:
			</div>
			
			<div class="credits_item_description">
				<a href="http://compsci.ca/v3/viewtopic.php?p=142539">Cet</a> algorithme (Licensé sous license libre zlib) a été utiliser afin de souligner les différences entre deux révisions d’un même modèle matériel.
			</div>

		</div>
	
	</div>
