<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

		<div class="back_button">
			<a rel="external" href="<?php echo $this->baseUrl."/hardware/catalogue/$lang/"?>" data-inline="true" data-icon="arrow-l" data-iconpos="left" data-theme="b" data-role="button"><?php echo gtext('back');?></a>
		</div>

		<div class="ui-body ui-body-c">
			<form action="<?php echo $this->currPage."/$lang".$clearViewStatus;?>" method="GET">
				
				<input type="search" name="search_string" id="basic" data-mini="true" value="<?php echo $search_string_form_value;?>"/>

				<button type="submit" data-theme="d"><?php echo gtext('Search');?></button>
			</form>
		</div>
		
		<!--if no device is found-->
		<?php if (strcmp($recordNumber,0) === 0) { ?>
		<div class="viewall_no_items_found">
			<?php echo gtext($notFoundString);?>...
		</div>
		<?php } ?>

		<!--loop-->
		<ul class="devices_list" data-dividertheme="b" data-role="listview">
			<?php if (count($table) > 0) { ?>
			<li data-role="list-divider"><h5><?php echo gtext("List of");?> <b><?php echo plural($this->controller);?></b></h5></li>
			<?php } ?>
			<?php foreach ($table as $item) {?>
			<li><a href="<?php echo $this->baseUrl."/".$this->controller."/view/$lang/".$item['hardware']['id_hard'].'/'.encodeUrl($item['hardware']['model']).$this->viewStatus;?>">
					<img src="<?php echo Hardware::getIconFromType($item['hardware']['type']);?>" alt="France" class="ui-li-icon" />
					<h3><?php echo $item['hardware']['model'];?></h3>
					
					<?php if ( strcmp($this->controller,'notebooks') !== 0 and strcmp($item['hardware']['other_names'],'') !== 0 ) { ?>
						<p><?php echo gtext("possible other names of the device");?>: <b><?php echo nl2br($item['hardware']['other_names']);?></b></p>
					<?php } ?>

					<?php if (strcmp($this->controller,'notebooks') === 0 or strcmp($this->controller,'hostcontrollers') === 0) { ?>
					<p>
						<?php if (strcmp($this->controller,'notebooks') === 0) { ?>
						<?php echo gtext("subtype");?> (<?php echo gtext("notebook");?>, <?php echo gtext("netbook");?>, <?php echo gtext("motherboard");?>, <?php echo gtext("tablet");?>)
						<?php } else { ?>
						<?php echo gtext("subtype");?> (<?php echo $subtypeHelpLabel;?>)
						<?php } ?>
						: <b><?php echo translate_and_gtext($item['hardware']['subtype']);?></b>
					</p>
					<?php } ?>
					
					<p><?php echo gtext("vendor");?>: <b><?php echo betterVendor($item['hardware']['vendor']);?></b></p>
			
					<p><?php echo gtext("year of commercialization");?>: <b><?php echo gtext($item['hardware']['comm_year']);?></b></p>

					<?php if (strcmp($this->controller,'notebooks') !== 0 ) { ?>
					<p><?php echo gtext("interface");?>: <b><?php echo gtext(translate($item['hardware']['interface']));?></b></p>
					<?php } ?>
					
					<p>
						<?php if (strcmp($this->controller,'printers') === 0 or strcmp($this->controller,'scanners') === 0 or strcmp($this->controller,'notebooks') === 0) { ?>
							<?php echo gtext("compatibility with free software");?>:
						<?php } else if (strcmp($this->controller,'videocards') === 0 ) { ?>
							<?php echo gtext("how does it work with free software?");?>
						<?php } else { ?>
							<?php echo gtext("does it work with free software?");?>
						<?php } ?>
						<b><?php echo gtext(translate($item['hardware'][$worksField]));?></b>
					</p>

					<?php if (strcmp($this->controller,'printers') === 0 ) { ?>
						<p><?php echo gtext("does it adopt any techniques to track users?");?> <b><?php echo gtext($item['hardware']['it_tracks_users']);?></b>
						</p>
					<?php } ?>
				</a>
			</li>
			<?php } ?>
		</ul>

		<div class="ui-grid-a page_list">
			<?php if ( $recordNumber > 10 ) { ?>
			<?php if ($this->viewArgs['page'] > 1) { ?>
				<div class="ui-block-a"><a rel="external" href="<?php echo $this->baseUrl."/".$this->controller."/catalogue/$lang/".($this->viewArgs['page']-1)."?search_string=".$this->viewArgs['search_string'];?>" data-inline="true" data-icon="arrow-l" data-theme="b" data-role="button"><?php echo gtext('previous');?></a></div>
			<?php } ?>
			<?php if ($this->viewArgs['page'] < $numberOfPages) { ?>
				<div style="text-align:right;" class="ui-block-b"><a rel="external" href="<?php echo $this->baseUrl."/".$this->controller."/catalogue/$lang/".($this->viewArgs['page']+1)."?search_string=".$this->viewArgs['search_string'];?>" data-inline="true" data-icon="arrow-r" data-iconpos="right" data-theme="b" data-role="button"><?php echo gtext('next');?></a></div>
			<?php } ?>
			<?php } ?>
		</div>