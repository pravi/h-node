<?php if (!defined('EG')) die('Direct access not allowed!'); ?>
<!DOCTYPE html>
<html>
<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>
<?php
$u = new UsersModel();
$hw = new HardwareModel();
$wiki = new WikiModel();
$translations = array('insert'=>'inserted','update'=>'updated');
$currPos = $querySanitized ? $this->controller."/".$this->action : 'home/index';
?>
<head>

	<title><?php echo $title;?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="description" content="free software project with the aim of collecting information about the hardware that works with a fully free operating system" />
	<meta name="keywords" content="hardware database free software GNU Linux distribution wiki users freedom" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
	<script type="text/javascript" src="<?php echo $this->baseUrl;?>/Public/Js/jquery/jquery-1.7.1.min.js"></script>

	<script type="text/javascript">
		$(document).bind("mobileinit", function(){
			 $.mobile.ajaxEnabled = false;
		});
	</script>

	<script type="text/javascript" src="<?php echo $this->baseUrl;?>/Public/Js/jquery/jquery.mobile-1.1.0.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl;?>/Public/Js/jquery/jquery.mobile-1.1.0.css">
	
	<script type="text/javascript" src="<?php echo $this->baseUrl;?>/Public/Js/functions.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl?>/Public/Css/mobile.css">

	<style type='text/css'>
	.ui-icon-current-lang
	{
		background-image: url(<?php echo $this->baseUrl;?>/Public/Img/Famfamfam/<?php echo $langIcon;?>);
		background-position:1px 4px;
	}
	.ui-icon-go-to-hardware
	{
		background-image: url(<?php echo $this->baseUrl;?>/Public/Img/Crystal/hardware.png);
		background-position:1px 0px;
	}
	.ui-icon-go-to-home
	{
		background-image: url(<?php echo $this->baseUrl;?>/Public/Img/Crystal/home.png);
		background-position:1px 1px;
	}
	</style>
	
	<script type="text/javascript">
	
		var base_url = "<?php echo $this->baseUrl;?>";
		var curr_lang = "<?php echo $lang;?>";
		var csrf_token = "<?php echo $token;?>";

		$(document).ready(function() {

			animateTabs(curr_lang);

		});

	</script>
	
</head>
<body>


<div data-role="page" data-theme="d">
	<div data-role="header">
		<div class="ui-grid-a">
			<div class="ui-block-a"><img width="150px" src="<?php echo $this->baseUrl;?>/Public/Img/title.png"></div>
			<div class="ui-block-b"></div>
		</div><!-- /grid-a -->

		<div data-theme='b' data-role="navbar" data-iconpos="left">
			<ul>
				<li><a data-icon="go-to-home" <?php echo $tm['home']; ?> href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a></li>
				<li><a data-icon="go-to-hardware" <?php echo $tm['hardware']; ?> href="<?php echo $this->baseUrl."/hardware/catalogue/$lang";?>">Hardware</a></li>
				<li><a data-icon="current-lang" data-rel="dialog" href="#language-dialog"><?php echo $langLabel;?></a></li>
			</ul>
		</div><!-- /navbar -->

	</div>
