<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
	
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; Hardware
		</div>
	
		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/H2O/computer-laptop.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/notebooks/catalogue/<?php echo $lang;?>"><?php echo gtext("Notebooks");?>, <?php echo gtext("netbooks");?>, <?php echo gtext("tablet PC");?></a>
		</div>
		
		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/H2O/network-wireless.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/wifi/catalogue/<?php echo $lang;?>"><?php echo gtext("Wifi cards");?></a>
		</div>
		
		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/1282042718_hardware.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/videocards/catalogue/<?php echo $lang;?>"><?php echo gtext("Video cards");?></a>
		</div>
		
		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/H2O/printer.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/printers/catalogue/<?php echo $lang;?>"><?php echo gtext("Printers and multifunction");?></a>
		</div>
		
		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/H2O/scanner.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/scanners/catalogue/<?php echo $lang;?>"><?php echo gtext("Scanners");?></a>
		</div>
		
		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/usb.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/threegcards/catalogue/<?php echo $lang;?>"><?php echo gtext("3G cards");?></a>
		</div>
		
		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/H2O/audio-card.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/soundcards/catalogue/<?php echo $lang;?>"><?php echo gtext("Sound cards");?></a>
		</div>

		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/H2O/camera-web.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/webcams/catalogue/<?php echo $lang;?>"><?php echo gtext("Webcams");?></a>
		</div>

		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/H2O/preferences-system-bluetooth.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/bluetooth/catalogue/<?php echo $lang;?>"><?php echo gtext("Bluetooth devices");?></a>
		</div>

		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/cam_mount.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/acquisitioncards/catalogue/<?php echo $lang;?>"><?php echo gtext("TV/Video/FM acquisition cards");?></a>
		</div>

		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/fingerprint_icon.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/fingerprintreaders/catalogue/<?php echo $lang;?>"><?php echo gtext("Fingerprint readers");?></a>
		</div>

		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/H2O/network-wired.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/ethernetcards/catalogue/<?php echo $lang;?>"><?php echo gtext("Ethernet cards");?></a>
		</div>

		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/H2O/media-flash-sd-mmc.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/sdcardreaders/catalogue/<?php echo $lang;?>"><?php echo gtext("SD card readers");?></a>
		</div>

		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/modem.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/modems/catalogue/<?php echo $lang;?>"><?php echo gtext("Modems and ADSL cards");?></a>
		</div>
		
		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/1282042718_hardware.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/raidadapters/catalogue/<?php echo $lang;?>"><?php echo gtext("RAID adapters");?></a>
		</div>
		
		<div class="hardware_element">
			<img align="middle" class="hardware_element_image" src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/1282042718_hardware.png"><a class="hardware_element_link" href="<?php echo $this->baseUrl?>/hostcontrollers/catalogue/<?php echo $lang;?>"><?php echo gtext("Host Controllers");?></a>
		</div>
	</div>