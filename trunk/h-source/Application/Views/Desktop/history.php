<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<?php if (!$isDeleted) { ?>
	
		<div class="notebooks_viewall">
			
			<ul class="page_history">

				<?php if ($this->viewArgs['history_page'] === 1) { ?>
				<?php foreach ($rev1 as $rev) { ?>
				<li class="page_history_current_item"><b><?php echo gtext("Current revision");?>:</b> <?php echo smartDate($rev['hardware']['update_date']);?> <?php echo gtext('by');?> <?php echo $u->getLinkToUserFromId($rev['hardware']['updated_by']);?> (<a href="<?php echo $this->baseUrl."/".$this->controller."/differences/$lang/$id/0".$this->viewStatus;?>">diff</a>)</li>
				<?php } ?>
				<?php } ?>
				
				<?php foreach ($rev2 as $rev) { ?>
				<li class="page_history_item">
				
					<a href="<?php echo $this->baseUrl."/".$this->controller."/revision/$lang/".$rev['revisions']['id_rev'].$this->viewStatus;?>"><?php echo smartDate($rev['revisions']['update_date']);?></a> <?php echo gtext('by');?> <?php echo $u->getLinkToUserFromId($rev['revisions']['updated_by']);?>
				
					<?php if (strcmp($rev['revisions']['id_rev'],$firstRev) !== 0) {?>
					(<a href="<?php echo $this->baseUrl."/".$this->controller."/differences/$lang/$id/".$rev['revisions']['id_rev'].$this->viewStatus;?>">diff</a>)
					<?php } ?>
					
					<?php if ($islogged === 'yes') { ?>
					(<a href="<?php echo $this->baseUrl.'/'.$this->controller.'/climb/'.$lang.'/'.$rev['revisions']['id_rev'].'/'.$token.$this->viewStatus;?>"><?php echo gtext('Make current');?></a>)
					<?php } ?>
				
				</li>
				<?php } ?>
				
			</ul>
			
		</div>
		
		<div class="history_page_list">
			<?php echo gtext("page list");?>: <?php echo $pageList;?>
		</div>

	<?php } else { ?>

	<div style="clear:both;"></div>
	
	<?php } ?>