<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<script>
	
		$(document).ready(function() {
			
			dist_list_helper();
			
			$("#bb_code").markItUp(mySettings);
			
		});
		
	</script>

	<?php echo $notice;?>

	<div class="notebooks_insert_form">
		<form action="<?php echo $this->baseUrl."/notebooks/".$this->action."/$lang/$token".$this->viewStatus;?>" method="POST">
		
			<div class="edit_form">
			
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("vendor");?>:</div>
					<?php echo Html_Form::select('vendor',$values['vendor'],Notebooks::$vendors,"select_entry");?>
					<a href="<?php echo $this->baseUrl."/issues/viewall/$lang/1/$token";?>">Vendor not present?</a>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("model name");?>: <b>*</b><?php echo $modelNameLabel;?></div>
					<?php echo Html_Form::input('model',$values['model'],'input_entry');?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("subtype (notebook, netbook, tablet)");?>:</div>
					<?php echo Html_Form::select('subtype',$values['subtype'],Notebooks::$subtypeSelect,"select_entry");?>
				</div>

				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("architecture");?>:</div>
					<?php echo Html_Form::select('architecture',$values['architecture'],Notebooks::$architectureSelect,"select_entry");?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("does it have a free boot firmware (BIOS,UEFI,...) ?");?></div>
					<?php echo Html_Form::select('bios',$values['bios'],Notebooks::$biosSelect,"select_entry");?>
				</div>

				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("can free operating systems be installed?");?></div>
					<?php echo Html_Form::select('can_free_systems_be_installed',$values['can_free_systems_be_installed'],Notebooks::$installableSelect,"select_entry");?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("does the device prevent installing wifi cards not-approved by the vendor?");?><?php echo $preventWifiLabel;?></div>
					<?php echo Html_Form::select('prevent_wifi',$values['prevent_wifi'],Notebooks::$preventWifiSelect,"select_entry");?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("year of commercialization");?>:</div>
					<?php echo Html_Form::select('comm_year',$values['comm_year'],Hardware::getCommYears(),"select_entry");?>
				</div>
				
				<div class="form_entry td_with_distribution_checkboxes">
					<div class="entry_label"><?php echo gtext("GNU/Linux distribution used for the test");?>: <b>*</b></div>
					<?php include(ROOT . DS . APPLICATION_PATH . DS . 'Views' . DS . Params::$viewSubfolder. DS . 'noscript_distributions.php');?>
					<?php echo Html_Form::input('distribution',$values['distribution'],'input_entry input_distribution');?>
					<?php echo Distributions::getFormHtml();?>
				</div>
				
				<div class="form_entry hidden_x_explorer">
					<div class="entry_label"><?php echo gtext("compatibility with free software");?>:</div>
					<?php echo Html_Form::select('compatibility',$values['compatibility'],Notebooks::$compatibility,"select_entry");?>
					<a class="open_help_window" title="compatibility help page" target="blank" href="<?php echo $this->baseUrl."/wiki/page/$lang/Compatibility-classes#Notebooks";?>"><img class="top_left_images_help" src="<?php echo $this->baseUrl;?>/Public/Img/Acun/help_hint.png"></a>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("tested with the following kernel libre");?>:<br /><span class="entry_label_small"><?php echo gtext("Write a comma-separated list of kernel versions");?><br /><?php echo gtext("Example");?>: 2.6.35-28-generic, 2.6.38-11 </span></div>
					<?php echo Html_Form::input('kernel',$values['kernel'],'input_entry');?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("video card model");?>:</div>
					<?php echo Html_Form::input('video_card_type',$values['video_card_type'],'input_entry');?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("does the video card work?");?></div>
					<?php echo Html_Form::select('video_card_works',$values['video_card_works'],Notebooks::$videoSelect,"select_entry");?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("wifi model");?>:</div>
					<?php echo Html_Form::input('wifi_type',$values['wifi_type'],'input_entry');?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("does the wifi card work?");?></div>
					<?php echo Html_Form::select('wifi_works',$values['wifi_works'],Notebooks::$wifiSelect,"select_entry");?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("webcam model");?>:</div>
					<?php echo Html_Form::input('webcam_type',$values['webcam_type'],'input_entry');?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("does the webcam work?");?></div>
					<?php echo Html_Form::select('webcam_works',$values['webcam_works'],Notebooks::$webcamSelect,"select_entry");?>
				</div>
				
				<div class="form_entry">
					<div class="entry_label"><?php echo gtext("Description: (write here all the useful information)");?><?php echo $descriptionLabel;?></div>

					<?php if (isset($descriptionPreview)) { ?>
					<div class="description_preview_title"><?php echo gtext("Description entry preview");?>:</div>
					<div class="description_preview">
						<?php echo decodeWikiText($descriptionPreview); ?>
					</div>
					<?php } ?>
					
					<?php echo Html_Form::textarea('description',$values['description'],'textarea_entry','bb_code');?>
				</div>
				
				<?php echo $hiddenInput;?>

				<input type="submit" name="previewAction" value="Preview">
				<input type="submit" name="<?php echo $submitName;?>" value="Save">
				
				<div class="mandatory_fields_notice">
					<?php echo gtext("Fields marked with <b>*</b> are mandatory");?>
				</div>
				
			</div>

		</form>
	</div>