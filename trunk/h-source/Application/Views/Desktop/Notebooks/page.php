<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

		<?php if (!$isDeleted or $isadmin) { ?>
		
			<?php
			$tableName = (strcmp($this->action,'view') === 0) ? 'hardware' : 'revisions';
			$displayClass = ($isDeleted and $isadmin) ? 'display_none' : null;
			?>
		
			<?php foreach ($table as $item) { ?>
			<div class="notebooks_viewall <?php echo $displayClass;?>">
			
				<!--if revision-->
				<?php if (strcmp($this->action,'revision') === 0) { ?>
				<div class="revision_alert">
					<?php echo gtext("This is an old revision of this page, as edited by");?> <b><?php echo $u->getLinkToUserFromId($updated_by);?></b> <?php echo gtext('at');?> <b><?php echo smartDate($update_date); ?></b>. <?php echo gtext("It may differ significantly from the");?> <a href="<?php echo $this->baseUrl."/".$this->controller."/view/$lang/$id_hard/".$name.$this->viewStatus;?>"><?php echo gtext("Current revision");?></a>.
				</div>
				<?php } ?>
				
				<div class="notebook_model">
					<img align="top" class="catalogue_item_icon" src="<?php echo Hardware::getIconFromType($item[$tableName]['type']);?>"> <span class="span_model_name"><?php echo gtext("model");?>: <b><?php echo $item[$tableName]['model'];?></b><span class="model_id">(<?php echo gtext("model id");?>: <?php echo $id_hard;?>)</span></span>
					<?php if (strcmp($islogged,'yes') === 0 and  strcmp($this->action,'view') === 0) { ?>
					<span class="ask_for_removal_class"><a class="ask_for_removal_class_link" href="<?php echo $this->baseUrl;?>">ask for removal</a></span>
					<?php } ?>
				</div>

				<div class="notebook_vendor">
					<div class="inner_label"><?php echo gtext("subtype");?> (<?php echo gtext("notebook");?>, <?php echo gtext("netbook");?>, <?php echo gtext("motherboard");?>, <?php echo gtext("tablet");?>):</div>
					<div class="inner_value"><b><?php echo gtext($item[$tableName]['subtype']);?></b></div>
				</div>
				
				<div class="notebook_vendor">
					<div class="inner_label"><?php echo gtext("vendor");?>:</div>
					<div class="inner_value"><b><?php echo betterVendor($item[$tableName]['vendor']);?></b></div>
				</div>

				<div class="notebook_vendor">
					<div class="inner_label"><?php echo gtext("architecture");?>:</div>
					<div class="inner_value"><b><?php echo gtext(translate($item[$tableName]['architecture']));?></b></div>
				</div>
				
				<div class="notebook_vendor">
					<div class="inner_label"><?php echo gtext("does it have a free boot firmware (BIOS,UEFI,...) ?");?></div>
					<div class="inner_value"><b><?php echo translate_and_gtext($item[$tableName]['bios']);?></b></div>
				</div>

				<div class="notebook_vendor">
					<div class="inner_label"><?php echo gtext("can free operating systems be installed?");?></div>
					<div class="inner_value"><b><?php echo gtext($item[$tableName]['can_free_systems_be_installed']);?></b> <?php if (strcmp($item[$tableName]['can_free_systems_be_installed'],'no') === 0) echo "<span class='no_bold'>(".gtext("see the details inside the description entry").")</span>";?></div>
				</div>
				
				<div class="notebook_vendor">
					<div class="inner_label"><?php echo gtext("does the device prevent installing wifi cards not-approved by the vendor?");?></div>
					<div class="inner_value"><b><?php echo translate_and_gtext($item[$tableName]['prevent_wifi']);?></b> <?php if (strcmp($item[$tableName]['prevent_wifi'],'yes') === 0) echo "<span class='no_bold'>(".gtext("see the details inside the description entry").")</span>";?></div>
				</div>
				
				<div class="notebook_vendor">
					<div class="inner_label"><?php echo gtext("year of commercialization");?>:</div>
					<div class="inner_value"><b><?php echo gtext($item[$tableName]['comm_year']);?></b></div>
				</div>

				<div class="notebook_compatibility">
					<div class="inner_label"><?php echo gtext("compatibility with free software");?>:</div>
					<div class="inner_value"><b><?php echo gtext($item[$tableName]['compatibility']);?></b> <a class="open_help_window" target="blank" title="compatibility help page" href="<?php echo $this->baseUrl."/wiki/page/$lang/Compatibility-classes#Notebooks";?>"><img class="top_left_images_help" src="<?php echo $this->baseUrl;?>/Public/Img/Acun/help_hint.png"></a></div>
				</div>
				
				<div class="model_tested_on">
					<div class="inner_label"><?php echo gtext("tested on");?>:</div>
					<div class="inner_value"><b><?php echo Distributions::getName($item[$tableName]['distribution']);?></b></div>
				</div>
				
				<div class="notebook_kernel">
					<div class="inner_label"><?php echo gtext("tested with the following kernel libre");?>:</div>
					<div class="inner_value"><b><?php echo implode("<br />",explode(',',$item[$tableName]['kernel']));?></b></div>
				</div>
				
				<div class="notebook_kernel">
					<div class="inner_label"><?php echo gtext("video card model");?>:</div>
					<div class="inner_value"><b><?php echo $item[$tableName]['video_card_type'];?></b> (<?php echo gtext(Notebooks::$videoReverse[$item[$tableName]['video_card_works']]);?>)</div>
				</div>
				
				<div class="notebook_kernel">
					<div class="inner_label"><?php echo gtext("wifi model");?>:</div>
					<div class="inner_value"><b><?php echo $item[$tableName]['wifi_type'];?></b> (<?php echo gtext(Notebooks::$wifiReverse[$item[$tableName]['wifi_works']]);?>)</div>
				</div>
				
				<div class="notebook_kernel">
					<div class="inner_label"><?php echo gtext("webcam model");?>:</div>
					<div class="inner_value"><b><?php echo $item[$tableName]['webcam_type'];?></b> (<?php echo gtext(Notebooks::$webcamReverse[$item[$tableName]['webcam_works']]);?>)</div>
				</div>
				
				<div class="notebook_description">
					<div class="notebook_description_label"><?php echo gtext("Description");?>:</div>
					<div class="notebook_description_value"><?php echo decodeWikiText($item[$tableName]['description']);?></div>
				</div>
				
			</div>
			<?php } ?>
		
		<?php } ?>