<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>
	<div id="left">

		<?php if (!$isDeleted) { ?>
		
			<div class="position_tree_box">
				<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo $tree;?>
			</div>

			<div class="notebook_view_title">
				<?php echo gtext('Make this revision the current revision of the page'); ?> <b><?php echo $tree_name;?></b>
			</div>

			<div class="notebook_insert_link">
				<a title="Back to the history of the page <?php echo $tree_name;?>" href="<?php echo $this->baseUrl."/".$this->controller."/history/$lang/".$id_wiki.$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
			</div>

			<div class="wiki_external_box">

				<?php echo $notice;?>

				<div class="climb_form_ext_box">

					<form action="<?php echo $this->currPage."/$lang/$id_rev"; ?>" method="POST">
						<?php echo gtext('I want to make this revision the current revision');?>: <input type="submit" name="confirmAction" value="<?php echo gtext('Confirm');?>">
					</form>

				</div>

			</div>
			
		<?php } ?>