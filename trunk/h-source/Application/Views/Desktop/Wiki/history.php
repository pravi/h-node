<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">

		<?php if (!$isDeleted) { ?>
		
			<div class="position_tree_box">
				<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/wiki/page/$lang";?>">Wiki</a> &raquo; <a href="<?php echo $wiki->toWikiPage($id);?>"><?php echo $tree_name;?></a> &raquo; <?php echo gtext('History');?>
			</div>

			<div class="notebook_view_title">
				<?php echo gtext('History of the wiki page'); ?> <b><?php echo $tree_name;?></b>
			</div>

			<div class="notebook_insert_link">
				<a title="Back to the page <?php echo $tree_name;?>" href="<?php echo $wiki->toWikiPage($id);?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
			</div>

			<div class="wiki_external_box">

				<ul class="page_history">

					<?php if ($this->viewArgs['page'] === 1) { ?>
					<?php foreach ($rev1 as $rev) { ?>
					<li class="page_history_current_item"><b><?php echo gtext("Current revision");?>:</b> <?php echo smartDate($rev['wiki']['update_date']);?> <?php echo gtext("by");?> <?php echo $u->getLinkToUserFromId($rev['wiki']['created_by']);?> (<a href="<?php echo $this->baseUrl."/".$this->controller."/differences/$lang/$id/0".$this->viewStatus;?>">diff</a>)</li>
					<?php } ?>
					<?php } ?>

					<?php foreach ($rev2 as $rev) { ?>
					<li class="page_history_item">

						<a href="<?php echo $this->baseUrl."/".$this->controller."/revision/$lang/".$rev['wiki_revisions']['id_rev'].$this->viewStatus;?>"><?php echo smartDate($rev['wiki_revisions']['update_date']);?></a> <?php echo gtext("by");?> <?php echo $u->getLinkToUserFromId($rev['wiki_revisions']['created_by']);?>

						<?php if (strcmp($rev['wiki_revisions']['id_rev'],$firstRev) !== 0) {?>
						(<a href="<?php echo $this->baseUrl."/".$this->controller."/differences/$lang/$id/".$rev['wiki_revisions']['id_rev'].$this->viewStatus;?>">diff</a>)
						<?php } ?>

						<?php if ($islogged === 'yes' and !$isBlocked) { ?>
						(<a href="<?php echo $this->baseUrl.'/'.$this->controller.'/climb/'.$lang.'/'.$rev['wiki_revisions']['id_rev'].$this->viewStatus;?>"><?php echo gtext('Make current'); ?></a>)
						<?php } ?>

					</li>
					<?php } ?>

				</ul>

			</div>

			<div class="history_page_list">
				<?php echo gtext("page list");?>: <?php echo $pageList;?>
			</div>
			
		<?php } ?>

