<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/search/form/$lang";?>"><?php echo gtext('Search form');?></a> &raquo; <?php echo gtext('Results of the search');?>
		</div>
		
		<div class="notebook_view_title">
			<?php echo gtext("Results of the search");?>:
		</div>
			
		<div class="search_form">
			<?php
			$count = 0;
			foreach ($table as $row) {
			$count++;
			?>
			
			<div class="search_result_item">
				<div class="first_level">
					<div class="search_result_item_left"><?php echo gtext("model name");?>:</div> <div class="search_result_item_right"><b><a href="<?php echo $this->baseUrl."/".Hardware::getControllerFromType($row['hardware']['type'])."/view/$lang/".$row['hardware']['id_hard']."/".encodeUrl($row['hardware']['model']);?>"><?php echo $row['hardware']['model'];?></a></b></div>
				</div>
				<?php if ( strcmp($row['hardware']['other_names'],'') !== 0 ) { ?>
				<div class="first_level">
					<div class="search_result_item_left"><?php echo gtext("possible other names of the device");?>:</div> <div class="search_result_item_right"><b><?php echo nl2br($row['hardware']['other_names']);?></b></div>
				</div>
				<?php } ?>
				<div class="first_level">
					<div class="search_result_item_left"><?php echo gtext("model type");?>: </div> <div class="search_result_item_right"><b><?php echo $row['hardware']['type'];?></b></div>
				</div>
				<div class="first_level">
					<div class="search_result_item_left"><?php echo gtext("year of commercialization");?>: </div> <div class="search_result_item_right"><b><?php echo $row['hardware']['comm_year'];?></b></div>
				</div>
			</div>
			
			<?php } ?>
			
			<?php if ($count === 0) { ?>
				<?php if (strcmp($this->action,'pciid') === 0) { ?>
				
					<div class="lspci_item_not_found">
					<?php echo gtext("There are no devices in the database with the vendorid:productid code specified by you.");?>
						<div class="lspci_item_not_found_inner"><?php echo gtext("Would you like to add it to the database?");?></div>
						<ul class="insert_suggestion_list">
							<?php foreach ($stat as $type => $number) { ?>
								<li><img align="top" class="catalogue_item_icon" src="<?php echo Hardware::getIconFromType($type);?>"> <a href="<?php echo $this->baseUrl."/".Hardware::getControllerFromType($type)."/insert/$lang/$token";?>"><?php echo plural(Hardware::getControllerFromType($type));?></a></li>
							<?php } ?>
						</ul>
					</div>

				<?php } else { ?>
				<div class="search_result_item">
					<?php echo gtext("No devices found");?>.
					</div>
				<?php } ?>
			<?php } ?>
			
		</div>
		
		<?php if (strcmp($recordNumber,0) !== 0) { ?>
		<div class="history_page_list">
			<?php echo gtext("page list");?>: <?php echo $pageList;?>
		</div>
		<?php } ?>
		
	</div>
