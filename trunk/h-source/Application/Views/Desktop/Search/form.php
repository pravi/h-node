<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<script>
	
		$(document).ready(function() {
			
			$(".search_form table").append("<tr><td><input id=\"search_action_input\" type=\"submit\" name=\"action\" value=\"<?php echo gtext('Search');?>\"></td></tr>");
			
			$("#search_action_input").click(function(){

				var s_type = $("#search_type_input").attr("value");
				var s_model = $("#search_model_input").attr("value");
				var s_action = $("#search_action_input").attr("value");
			
				var s_url = "1/search/" + s_type + "/" + s_model;

				location.href="<?php echo $this->baseUrl."/search/results/$lang/";?>"+s_url;
				return false;
			});
			
		});
		
	</script>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo gtext('Search form');?>
		</div>
		
		<div class="notebook_view_title">
			<?php echo gtext("Search one device in the archive");?>:
		</div>
			
		<div class="search_form">
			<div class="search_form_label">
				<img class="search_img" src="<?php echo $this->baseUrl;?>/Public/Img/Glaze/viewmag.png"> <span class="span_search_desc"><?php echo gtext("search by");?> <b><?php echo gtext("model name");?></b></span>
			</div>
			<noscript>
				<div class="noscript_notice">
					you need javascript enabled in order to use the form below
				</div>
			</noscript>
			<form method="GET">
				<table>
					<tr>
						<td><?php echo gtext("hardware type");?>:</td>
						<td><?php echo Html_Form::select('type','',Hardware::getTypes(),"select_entry","search_type_input");?></td>
					</tr>
					<tr>
						<td><?php echo gtext("the model name contains");?>:</td>
						<td><?php echo Html_Form::input('model','','input_entry_search',"search_model_input");?></td>
					</tr>
				</table>
			</form>
		</div>

		<div class="search_form_pciid">
			<div class="search_form_label">
				<img class="search_img" src="<?php echo $this->baseUrl;?>/Public/Img/Glaze/viewmag.png"> <span class="span_search_desc"><?php echo gtext("search by");?> <b>vendorid:productid</b></span>
			</div>
			<form action="<?php echo $this->baseUrl."/search/pciid/".Lang::$current;?>" method="POST">
				<table>
					<tr>
						<td><?php echo gtext("VendorID:ProductID code of the device");?>:</td>
						<td><?php echo Html_Form::input('pciid','','input_entry_search');?></td>
					</tr>
					<tr>
						<td><input type="submit" name="search_pci" value="<?php echo gtext('Search');?>"></td>
					</tr>
				</table>
			</form>
		</div>

		<div class="search_form_lspci">
			<div class="search_form_label">
				<img class="search_img" src="<?php echo $this->baseUrl;?>/Public/Img/Glaze/viewmag.png"> <span class="span_search_desc"><?php echo gtext("analyze the output of the lspci command");?></span>
			</div>
			<form action="<?php echo $this->baseUrl."/search/lspci/".Lang::$current;?>" method="POST">
				<table>
					<tr>
						<td valign="top"><?php echo gtext("paste the output of the lspci command");?>:<div class="lspci_notice">lspci -vmmnn</div></td>
						<td><?php echo Html_Form::textarea('lspci',gtext('write here the output of lspci -vmmnn'),'textarea_entry_search');?></td>
					</tr>
					<tr>
						<td><input type="submit" name="search_pci" value="<?php echo gtext('Search');?>"></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
