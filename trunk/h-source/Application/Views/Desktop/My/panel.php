<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo gtext("control panel");?>
		</div>
		
		<?php if ($userBlocked) { ?>
		<div class="moderator_box">
			Your account has been blocked by an administrator of the website. You can neither insert/modify devices nor submit new issues or messages until some other moderator un-block your account.
		</div>
		<?php } ?>

		<?php if ($isadmin) { ?>
		<div id="admin_box">
			<div class="admin_box_title"><?php echo gtext("Special pages for administrators");?></div>
			<ul>
				<li><a href="<?php echo Url::getRoot('special/adminactions/'.$lang);?>"><?php echo gtext("Actions carried out by administrators");?></a></li>
				<li><a href="<?php echo Url::getRoot('special/deleted/'.$lang);?>"><?php echo gtext("List of hidden device pages");?></a></li>
				<li><a href="<?php echo Url::getRoot('special/notapproved/'.$lang);?>"><?php echo gtext("Device pages that have to be approved");?></a> ( <a class="not_bold" href="<?php echo $this->baseUrl."/rss/notapproved/$lang";?>"><?php echo gtext('you can also subscribe to the feed in order to receive the new modifications that need a moderation');?></a> )</li>
			</ul>
		</div>
		<?php } ?>

		<?php if ($ismoderator) { ?>
		<div id="moderator_box">
			<div class="moderator_box_title"><?php echo gtext("Special pages for moderators");?></div>
			<ul>
				<li><a href="<?php echo Url::getRoot('special/modactions/'.$lang);?>"><?php echo gtext("Actions carried out by moderators");?></a></li>
				<li><a href="<?php echo Url::getRoot('special/usersactions/'.$lang);?>"><?php echo gtext("Actions carried out by users");?></a></li>
			</ul>
		</div>
		<?php } ?>
		
		<ul class='panelApplicationList'>
			<li><a href="<?php echo Url::getRoot('meet/user/'.$lang.'/'.$username);?>"><?php echo gtext("Watch your public profile");?></a></li>
			<li><a href="<?php echo Url::getRoot('my/profile/'.$lang.'/'.$token);?>"><?php echo gtext("Edit your profile");?></a></li>
			<li><a href="<?php echo Url::getRoot('my/email/'.$lang.'/'.$token);?>"><?php echo gtext("Change your e-mail address");?></a></li>
			<li><a href="<?php echo Url::getRoot('my/password/'.$lang.'/'.$token);?>"><?php echo gtext("Change your password");?></a></li>
			<li><a href="<?php echo Url::getRoot('my/goodbye/'.$lang.'/'.$token);?>"><?php echo gtext("Delete your account");?></a></li>
		</ul>
		
	</div>