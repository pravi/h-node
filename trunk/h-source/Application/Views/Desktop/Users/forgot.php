<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
	
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo gtext("request new password");?>
		</div>
		
		<div class="new_account_title">
			<?php echo gtext("request new password");?>
		</div>
		
		<?php echo $notice;?>
		
		<form  class='formClass' action='<?php echo $this->currPage."/$lang";?>' method='POST'>
		
			<div class='formEntry'>
				<span class='entryLabel'><?php echo gtext("write your username");?>:</span>
				<input type="input" name="username" value="">
			</div>
			
			<div class="captcha_box">
				<img src="<?php echo $this->baseUrl?>/image/captcha">
			</div>
			
			<div class='formEntry'>
				<span class='entryLabel'><?php echo gtext("write the code above");?>:</span>
				<input type="input" name="captcha" value="">
			</div>
			
			<div class='inputEntry'>
				<input id='insertAction' type='submit' name='forgotAction' value='<?php echo gtext("request new password");?>'>
			</div>
			
		</form>
	
	</div>