<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

<script type="text/javascript">

	$(function(){
		
		$("#suggest_dialog").css("display","block");
		
		// Dialog			
		$('#suggest_dialog').dialog({
			autoOpen: false,
			width: 500,
		});
		
		$('#suggest_dialog').dialog('open');
			
	});
</script>

<div id="suggest_dialog" title="Insert new hardware">
	<p><?php echo gtext("Thanks for helping the h-node project and the free software movement!");?></p>
	<p><?php echo gtext("You have just inserted a new notebook into the database. Can you please insert its devices separately too? Thanks!");?></p>
</div>