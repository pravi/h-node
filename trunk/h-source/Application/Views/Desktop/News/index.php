<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">

		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo gtext("News");?>
		</div>

		<div class="news_external_box">
			<?php foreach ($table as $row) { ?>
			<div class="news_item">
				<div class="news_item_title">
					<?php echo $row['news']['title'];?>
				</div>
				<div class="news_item_date">
					<?php echo smartDate($row['news']['creation_date']);?>
				</div>
				<div class="news_item_message">
					<?php echo decodeWikiText($row['news']['message']);?>
				</div>
			</div>
			<?php } ?>
		</div>

		<?php if ($recordNumber > 10) { ?>
		<div class="history_page_list_news">
			<?php echo $pageList;?>
		</div>
		<?php } ?>
		
	</div>
