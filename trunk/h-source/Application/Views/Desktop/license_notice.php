<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div class="top_licence_notice">
		<div><b><?php echo gtext("License information");?>:</b></div>
		<?php echo License::getSubmissionNotice();?>
	</div>
	<?php if ($islogged === 'no') { ?>
	<div class="not_logged_license">
		<?php echo gtext("You are not a registered user or you have not logged in. Your contribution won't be published until an administrator approves it. If you want your contributions to be automatically published please log in or create an account.");?>
	</div>
	<?php } ?>
	<div class="box_module_how_to_compile"><?php echo $howToCompileLabel;?></div>