<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

		<div class="viewall_popup_menu_box_external">
			<div class="viewall_popup_menu_box">
				<?php echo $popup;?>
			</div>
			<div class="viewall_popup_menu_status">
				<?php echo $popupLabel;?>
			</div>
		</div>

		<?php if (strcmp($this->controller,'notebooks') === 0 ) { ?>
		<div class="viewall_popup_menu_box_external more_filters">
			<div class="viewall_popup_menu_box">
				<?php echo $popupMore;?>
			</div>
			<div class="viewall_popup_menu_status">
				<div class="viewall_popup_menu_status_item"><?php echo translate_and_gtext($this->viewArgs['bios'])?></div>
				<div class="viewall_popup_menu_status_item"><?php echo translate_and_gtext($this->viewArgs['architecture']);?></div>
			</div>
		</div>
		<?php } ?>

		<div class="model_string_search_form">
			<form action="<?php echo $this->currPage."/$lang".$clearViewStatus;?>" method="GET">
				<?php echo gtext("model name");?> <input type="text" value="<?php echo $search_string_form_value;?>" name="search_string">
				<a href="<?php echo $this->currPage."/$lang".$clearViewStatus;?>"><img src="<?php echo $this->baseUrl."/Public/Img/Crystal/button_cancel.png";?>" /></a>
				<input type="submit" value="<?php echo gtext('Search');?>" name="submit">
			</form>
		</div>
		
		<!--if no notebooks found-->
		<?php if (strcmp($recordNumber,0) === 0) { ?>
		<div class="viewall_no_items_found">
			<?php echo gtext($notFoundString);?>...
		</div>
		<?php } ?>
		
		<!--loop-->
		<?php foreach ($table as $item) {?>
		<div class="model_viewall">

			<div class="notebook_model">
				<img align="top" class="catalogue_item_icon" src="<?php echo Hardware::getIconFromType($item['hardware']['type']);?>"> <span class="span_model_name"><?php echo gtext("model");?>: <b><?php echo $item['hardware']['model'];?></b></span>
			</div>

			<?php if ( strcmp($this->controller,'notebooks') !== 0 and strcmp($item['hardware']['other_names'],'') !== 0 ) { ?>
			<div class="notebook_vendor">
				<div class="inner_label"><?php echo gtext("possible other names of the device");?>:</div>
				<div class="inner_value"><?php echo nl2br($item['hardware']['other_names']);?></div>
			</div>
			<?php } ?>
			
			<?php if (strcmp($this->controller,'notebooks') === 0 or strcmp($this->controller,'hostcontrollers') === 0) { ?>
			<div class="notebook_vendor">
				<?php if (strcmp($this->controller,'notebooks') === 0) { ?>
				<div class="inner_label"><?php echo gtext("subtype");?> (<?php echo gtext("notebook");?>, <?php echo gtext("netbook");?>, <?php echo gtext("motherboard");?>, <?php echo gtext("tablet");?>):</div>
				<?php } else { ?>
				<div class="inner_label"><?php echo gtext("subtype");?> (<?php echo $subtypeHelpLabel;?>):</div>
				<?php } ?>
				<div class="inner_value"><b><?php echo translate_and_gtext($item['hardware']['subtype']);?></b></div>
			</div>
			<?php } ?>
			
			<div class="notebook_vendor">
				<div class="inner_label"><?php echo gtext("vendor");?>:</div>
				<div class="inner_value"><?php echo betterVendor($item['hardware']['vendor']);?></div>
			</div>
			
			<div class="notebook_vendor">
				<div class="inner_label"><?php echo gtext("year of commercialization");?>:</div>
				<div class="inner_value"><b><?php echo gtext($item['hardware']['comm_year']);?></b></div>
			</div>

			<?php if (strcmp($this->controller,'notebooks') !== 0 ) { ?>
			<div class="notebook_vendor">
				<div class="inner_label"><?php echo gtext("interface");?>:</div>
				<div class="inner_value"><b><?php echo gtext(translate($item['hardware']['interface']));?></b></div>
			</div>
			<?php } ?>
			
			<div class="notebook_kernel">
				<?php if (strcmp($this->controller,'printers') === 0 or strcmp($this->controller,'scanners') === 0 or strcmp($this->controller,'notebooks') === 0) { ?>
				<div class="inner_label"><?php echo gtext("compatibility with free software");?>:</div>
				<?php } else if (strcmp($this->controller,'videocards') === 0 ) { ?>
				<div class="inner_label"><?php echo gtext("how does it work with free software?");?></div>
				<?php } else { ?>
				<div class="inner_label"><?php echo gtext("does it work with free software?");?></div>
				<?php } ?>
				<div class="inner_value"><b><?php echo gtext(translate($item['hardware'][$worksField]));?></b></div>
			</div>

			<?php if (strcmp($this->controller,'printers') === 0 ) { ?>
			<div class="notebook_kernel">
				<div class="inner_label"><?php echo gtext("does it adopt any techniques to track users?");?></div>
				<div class="inner_value"><b><?php echo gtext($item['hardware']['it_tracks_users']);?></b></div>
			</div>
			<?php } ?>
			
			<div class="notebook_view_link">
				<a href="<?php echo $this->baseUrl."/".$this->controller."/view/$lang/".$item['hardware']['id_hard'].'/'.encodeUrl($item['hardware']['model']).$this->viewStatus;?>"><?php echo gtext("view the other specifications");?></a>
			</div>
			
		</div>
		<?php } ?>
		
		<?php if (strcmp($recordNumber,0) !== 0) { ?>
		<div class="history_page_list">
			<?php echo gtext("page list");?>: <?php echo $pageList;?>
		</div>
		<?php } ?>