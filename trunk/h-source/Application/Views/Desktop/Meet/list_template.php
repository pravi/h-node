<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
	
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/meet/user/$lang/$meet_username";?>">meet <b><?php echo $meet_username;?></b></a> &raquo; <a href="<?php echo $this->baseUrl."/meet/contributions/$lang/$meet_username";?>">contributions</a> &raquo; <?php echo $tree_last_string;?>
		</div>
		
		<div class="contrib_explain_box">
			<?php echo $page_explanation_title;?> <?php echo $meet_username;?>
		</div>
		
		<div class="external_users_contrib">
			<?php foreach ($table as $item) {?>
			<div class="users_contrib_item">

				<div class="contribution_item">
					<?php include(ROOT . '/Application/Views/' .$subfolder. "/Meet/".$this->action.".php");?>
				</div>
				
			</div>
			<?php } ?>
		</div>

		<?php if (strcmp($this->action,'hardware') !== 0) {?>
		<div class="history_page_list">
			<?php echo gtext("page list");?>: <?php echo $pageList;?>
		</div>
		<?php } ?>
	</div>