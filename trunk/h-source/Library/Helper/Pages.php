<?php

// EasyGiant is a PHP framework for creating and managing dynamic content
//
// Copyright (C) 2009 - 2014  Antonio Gallo (info@laboratoriolibero.com)
// See COPYRIGHT.txt and LICENSE.txt.
//
// This file is part of EasyGiant
//
// EasyGiant is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// EasyGiant is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with EasyGiant.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

//Helper class to create the HTML of the page list
class Helper_Pages extends Helper_Html
{

	protected $_urlViewAction; //url of the current page
	protected $_currentPage; //number of the page
	protected $_numbOfPages; //number of pages
	protected $_variableArg = ''; //value of the $viewArgs key that has to be modified

	public $previousString = null; //string of the link to the previous page
	public $nextString = null; //string of the link to the next page
	public $showNext = true;
	public $showPrev = true;
	
	//instance of Lang_{language}_Generic
	public $strings = null;
	
	public function __construct()
	{
		//get the generic language class
		$this->strings = Factory_Strings::generic(Params::$language);
	}
	
	public function build($urlViewAction = '' , $variableArg = 'page', $previousString = 'previous', $nextString = 'next')
	{
		$this->_variableArg = $variableArg;
		$this->_urlViewAction =$urlViewAction; //url of the controller and (/) main action
		$this->previousString = $this->strings->gtext($previousString);
		$this->nextString = $this->strings->gtext($nextString);
	}

	//return the number of pages
	public function getNumbOfPages()
	{
		return $this->_numbOfPages;
	}

	//get the limit of the select query clause
	public function getLimit($currentPage,$recordNumber,$recordPerPage)
	{
		$this->_currentPage = $currentPage;
		$this->_numbOfPages=(($recordNumber%$recordPerPage)===0) ? (int) ($recordNumber/$recordPerPage) : ((int) ($recordNumber/$recordPerPage))+1;
		$start=(($currentPage-1)*$recordPerPage);
		return "$start,$recordPerPage";
	}

	//return the page list string
	public function render($pageNumber,$numberOfPages)
	{
		$pageList = null;
		if ($this->showPrev)
		{
			$pageList .= $this->pageLink($this->_currentPage-1,$this->previousString);
		}
		$pageList .= $this->recursiveLink($pageNumber,$numberOfPages);
		if ($this->showNext)
		{
			$pageList .= $this->pageLink($this->_currentPage+1,$this->nextString);
		}
		return $pageList;
	}

	//recorsive function in order to write the page list
	public function recursiveLink($pageNumber,$numberOfPages)
	{
		
		if ($numberOfPages === 0) return null;
		
		if ($numberOfPages === 1) {
			return $this->pageLink($pageNumber);
		} else {
			return $this->pageLink($pageNumber) . $this->recursiveLink($pageNumber+1,$numberOfPages-1);
		}
	}

	public function pageLink($pageNumber, $string = null) {
		if ($pageNumber > 0 and $pageNumber <= $this->_numbOfPages) {
			return $this->html($pageNumber,$string);
		} else {
			return null;
		}
	} 

	//return the html link
	public function html($pageNumber,$string = null) {
		if (isset($string)) {
			$strNumber = $string;
			if (strcmp($pageNumber,"1") === 0)
			{
				$strClass = "class='itemListPage previous_page'";
			}
			else
			{
				$strClass = "class='itemListPage next_page'";
			}
		} else {
			if (strcmp($pageNumber,$this->_currentPage) === 0)
			{
				$strNumber = $pageNumber;
				$strClass = "class='currentPage'";
			}
			else
			{
				$strNumber = $pageNumber;
				$strClass = "class='itemListPage'";
			}
		}
		$this->viewArgs[$this->_variableArg] = $pageNumber;
		$viewStatus = Url::createUrl($this->viewArgs);
		$href= Url::getRoot(null) . $this->_urlViewAction .$viewStatus;
		return $this->getATag($href,$strNumber,$strClass);
	}

	//get the HTMl of the tag
	//$href: href of the link
	//$text: the text of the link
	//$strClass: the class of the link
	public function getATag($href,$text,$strClass)
	{
		return "<a $strClass href='$href'>$text</a>";
	}

}