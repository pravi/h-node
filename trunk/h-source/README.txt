
== Introduction ==

h-source is a web software that can be used to build a community of people that want to share their hardware information. It is based on the EasyGiant PHP framework (www.easygiant.org) and can be considered an EasyGiant application


== Requirements ==

* php5-gd
* php5-cli


== Installation and configuration ==

In order to use h-source you have to install a web server that supports the URL rewrite feature (such as the Apache server with the mod_rewrite module installed), PHP 5.2 or newer and Mysql 5 or newer.


Extract the tarball inside a folder of your filesystem.

You have to create the MySQL database used by h-source. The tables of the database that have to be created are written inside the file tables.sql. A way to carry out this operation is the following:

Open a terminal and type:

	mysql -u <username> -p
	
where <username> is the database user. Write the password and type the following command:

	create database <db>;
	
where <db> is the database name you want to use. Close Mysql by typing the following command:

	exit
	
Now you have to move inside the folder that contains the tables.sql file and type the following command:

	mysql -u username -ppassword <db> < tables.sql
	
where username and password have to be changed with the real username and password. <db> is the name of the database just created.

Now you have created the database named <db>

This will also create the default user having the following credentials:

username: admin
password: admin

You will be able to login using the admin user and change the website preferences and the admin password.

Open the Config/Config.php file

You have to set the following constants:

	DB: name of the database (the database just created)
	USER: database user
	PWD: database password
	HOST: name of the mysql server (usually localhost)

	DOMAIN_NAME: the domain name of the website (use localhost or create a virtual host in your web server)

Now you have to copy the extracted h-source files inside the DocumentRoot of your server. You can choose to leave all the files inside the DocumentRoot or not, see the explanations in this page (http://www.easygiant.org/learn/index/6).

You have now to assure that the allowOverride directive is active in the configuration file of your Apache server. You have to activate the allowOverride directive for the folder inside which you have saved the h-source files (and the index.php file). If you have not activated the allowOverride directive, then your URL won't be interpreted by the mod_rewrite module (you won't be able to use h-source). In a GNU/Linux system the apache configuration file is usually saved at /etc/apache2/apache2.conf.

Direct your browser to the following URL:

	http://DOMAIN_NAME/
	
where DOMAIN_NAME is the domain name you have previously set (perhaps localhost). You should see the home page of the h-source software.



== Account issues ==

If you want that the system can send e-mails you have to specify the name of the SMTP server that has to be used. Open the file Application/Include/params.php and set the following static attributes of the Website class:

	$mailServer = "";  //set the mail server (only useful if $useSMTP = true)
	
	$generalMail = "";  //set the username of your mail account (always needed)
	
	$mailPassword = "";  //set the password of your mail account (only useful if $useSMTP = true)

	$fromEmail = "noreply@h-node.org"; //this is the "from address e-mail" used inside the mails sent by h-source (example: confirmation requesta e-mail, change password e-mail, ...)

	$useSMTP = true;  //if you want to use a SMTP account. It can be true or false. Set $useSMTP to false if you want that the software rely on the mail() PHP function
		
You can also set these constants:

	$generalName = "";  //the string that you want to use inside the <title> tag of your website
	
	$projectName = "";  //the name of your project 


== Change the configuration of some parts of the template (right column, top notices, top menu, ...) ==

Modify the file config.xml inside the ROOT folder of your h-source installation.

If you want to change the position of the config.xml file change the following static attribute of the Website class inside the 
Application/Include/params.php file:
	
	static public $xmlConfigFileFolder = ROOT; //the constant ROOT contains the path to the root folder of your installaton of h-source

== Change the homepage ==


Modify the PHP files inside the folder Application/Views/Home


== Change the hardware ==


Modify the PHP file Application/Include/hardware.php


== Change the allowed distributions ==


Modify the PHP file Application/Include/distributions.php


== Change the allowed languages ==


Modify the PHP file Application/Include/languages.php


== How to fill the vendors entry ==

After the installation you have to fill the MySQL table containing the vendors. To carry out this task you need to download the proper scripts using the following command:
	
	svn co svn://svn.savannah.nongnu.org/h-source/trunk/scripts/vendorid
	
Move into the just downloaded vendorid folder. Open the file connect.php and modify the following variables:

	$host		=	'name of the mysql server (the same used for h-source)';
	$user		=	'database user (the same used for h-source)';
	$password	=	'database pasword (the same used for h-source)';
	$dbName		=	'database name (the same used for h-source)';

You will find two file containing the list of vendors: pci.ids and usb.ids. If you want more up-to-date versions of those files then delete them and write the following commands (or download those files from the URLs indicated below):
	
	command to get pci.ids (remember to delete the old pci.ids file): 
		
		wget pciids.sourceforge.net/v2.2/pci.ids
	
	command  to get usb.ids (remember to delete the old pci.ids file):
		
		wget http://www.linux-usb.org/usb.ids

Be sure you have installed php5-cli, then write the following command:
	
	php5 insert_vendors.php

It will fill the vendors MySql table. It will take a bit.


== Learn the EasyGiant PHP framework ==

Since h-source is an application of the EasyGiant PHP framework you can use the EasyGiant features in order to modify the h-source source code. Visit www.easygiant.org


// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

