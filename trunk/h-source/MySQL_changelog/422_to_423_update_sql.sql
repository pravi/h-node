-- Apply these queries in order to update your database from revision 422 to revision 422

drop table distros;

create table distros (
	id_distro INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	creation_date timestamp default CURRENT_TIMESTAMP,
	clean_name varchar(200) CHARACTER SET utf8 not null,
	full_name varchar(200) CHARACTER SET utf8 not null,
	id_order INT UNSIGNED NOT NULL
)engine=innodb;

insert into distros (clean_name, full_name, id_order) values ('blag_90001', 'BLAG 90001', 1);
insert into distros (clean_name, full_name, id_order) values ('blag_120000', 'BLAG 120000', 2);
insert into distros (clean_name, full_name, id_order) values ('blag_140000', 'BLAG 1400000', 3);
insert into distros (clean_name, full_name, id_order) values ('debian_testing', 'Debian GNU/Linux Testing', 4);
insert into distros (clean_name, full_name, id_order) values ('debian_unstable', 'Debian GNU/Linux Unstable', 5);
insert into distros (clean_name, full_name, id_order) values ('debian_6', 'Debian GNU/Linux 6 squeeze', 6);
insert into distros (clean_name, full_name, id_order) values ('debian_7', 'Debian GNU/Linux 7 wheezy', 7);
insert into distros (clean_name, full_name, id_order) values ('dragora_1_1', 'Dragora 1.1', 8);
insert into distros (clean_name, full_name, id_order) values ('dragora_2_0', 'Dragora 2.0 Ardi', 9);
insert into distros (clean_name, full_name, id_order) values ('dragora_2_2', 'Dragora 2.2 Rafaela', 10);
insert into distros (clean_name, full_name, id_order) values ('dynebolic_2_5_2', 'Dyne:bolic 2.5.2 DHORUBA', 11);
insert into distros (clean_name, full_name, id_order) values ('dynebolic_3_0_X', 'Dyne:III 3.0.X MUNIR', 12);
insert into distros (clean_name, full_name, id_order) values ('gnewsense_2_3', 'gNewSense 2.3 Deltah', 13);
insert into distros (clean_name, full_name, id_order) values ('gnewsense_3_0', 'gNewSense 3.0 Metad (beta)', 14);
insert into distros (clean_name, full_name, id_order) values ('gnewsense_3_0_parkes', 'gNewSense 3.0 Parkes', 15);
insert into distros (clean_name, full_name, id_order) values ('musix_2_0', 'Musix GNU+Linux 2.0 R0', 16);
insert into distros (clean_name, full_name, id_order) values ('parabola', 'Parabola GNU/Linux', 17);
insert into distros (clean_name, full_name, id_order) values ('trisquel_3_5', 'Trisquel 3.5 Awen', 18);
insert into distros (clean_name, full_name, id_order) values ('trisquel_4_0', 'Trisquel 4.0 Taranis', 19);
insert into distros (clean_name, full_name, id_order) values ('trisquel_4_5', 'Trisquel 4.5 Slaine', 20);
insert into distros (clean_name, full_name, id_order) values ('trisquel_5_0', 'Trisquel 5.0 Dagda', 21);
insert into distros (clean_name, full_name, id_order) values ('trisquel_5_5', 'Trisquel 5.5 Brigantia', 22);
insert into distros (clean_name, full_name, id_order) values ('trisquel_6_0', 'Trisquel 6.0 Toutatis', 23);
insert into distros (clean_name, full_name, id_order) values ('ututo_xs_2009', 'UTUTO XS 2009', 24);
insert into distros (clean_name, full_name, id_order) values ('ututo_xs_2010', 'UTUTO XS 2010', 25);
insert into distros (clean_name, full_name, id_order) values ('ututo_xs_2012_04', 'UTUTO XS 2012.04', 26);
insert into distros (clean_name, full_name, id_order) values ('venenux_0_8', 'VENENUX 0.8', 27);
insert into distros (clean_name, full_name, id_order) values ('venenux_0_8_2', 'VENENUX-EC 0.8.2', 28);
insert into distros (clean_name, full_name, id_order) values ('venenux_0_9', 'VENENUX 0.9', 29);
