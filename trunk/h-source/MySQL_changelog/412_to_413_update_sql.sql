-- NB: only if you are currently working with the revision 412 (otherwise apply all the steps to get to revision 412)!!
-- Apply these queries in order to update your database and work with revision 413

create table distros (
	id_distro INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	creation_date timestamp default CURRENT_TIMESTAMP,
	clean_name varchar(200) CHARACTER SET utf8 not null,
	full_name varchar(200) CHARACTER SET utf8 not null,
	id_order INT UNSIGNED NOT NULL
)engine=innodb;

insert into distros (clean_name, full_name, id_order) values ('blag_90001', 'BLAG 90001', 1);
insert into distros (clean_name, full_name, id_order) values ('blag_120000', 'BLAG 120000', 2);
insert into distros (clean_name, full_name, id_order) values ('blag_140000', 'BLAG 1400000', 3);
insert into distros (clean_name, full_name, id_order) values ('dragora_1_1', 'Dragora 1.1', 4);
insert into distros (clean_name, full_name, id_order) values ('dragora_2_0', 'Dragora 2.0 Ardi', 5);
insert into distros (clean_name, full_name, id_order) values ('dragora_2_2', 'Dragora 2.2 Rafaela', 6);
insert into distros (clean_name, full_name, id_order) values ('dynebolic_2_5_2', 'Dyne:bolic 2.5.2 DHORUBA', 7);
insert into distros (clean_name, full_name, id_order) values ('dynebolic_3_0_X', 'Dyne:III 3.0.X MUNIR', 8);
insert into distros (clean_name, full_name, id_order) values ('gnewsense_2_3', 'gNewSense 2.3 Deltah', 9);
insert into distros (clean_name, full_name, id_order) values ('gnewsense_3_0', 'gNewSense 3.0 Metad (beta)', 10);
insert into distros (clean_name, full_name, id_order) values ('gnewsense_3_0_parkes', 'gNewSense 3.0 Parkes', 11);
insert into distros (clean_name, full_name, id_order) values ('musix_2_0', 'Musix GNU+Linux 2.0 R0', 12);
insert into distros (clean_name, full_name, id_order) values ('parabola', 'Parabola GNU/Linux', 13);
insert into distros (clean_name, full_name, id_order) values ('trisquel_3_5', 'Trisquel 3.5 Awen', 14);
insert into distros (clean_name, full_name, id_order) values ('trisquel_4_0', 'Trisquel 4.0 Taranis', 15);
insert into distros (clean_name, full_name, id_order) values ('trisquel_4_5', 'Trisquel 4.5 Slaine', 16);
insert into distros (clean_name, full_name, id_order) values ('trisquel_5_0', 'Trisquel 5.0 Dagda', 17);
insert into distros (clean_name, full_name, id_order) values ('trisquel_5_5', 'Trisquel 5.5 Brigantia', 18);
insert into distros (clean_name, full_name, id_order) values ('trisquel_6_0', 'Trisquel 6.0 Toutatis', 19);
insert into distros (clean_name, full_name, id_order) values ('ututo_xs_2009', 'UTUTO XS 2009', 20);
insert into distros (clean_name, full_name, id_order) values ('ututo_xs_2010', 'UTUTO XS 2010', 21);
insert into distros (clean_name, full_name, id_order) values ('ututo_xs_2012_04', 'UTUTO XS 2012.04', 22);
insert into distros (clean_name, full_name, id_order) values ('venenux_0_8', 'VENENUX 0.8', 23);
insert into distros (clean_name, full_name, id_order) values ('venenux_0_8_2', 'VENENUX-EC 0.8.2', 24);
insert into distros (clean_name, full_name, id_order) values ('venenux_0_9', 'VENENUX 0.9', 25);